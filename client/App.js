// import React from 'react';
// import { NavigationContainer } from '@react-navigation/native';
// import { createStackNavigator } from '@react-navigation/stack';
// import { StatusBar } from 'expo-status-bar';
// import { StyleSheet, SafeAreaView } from 'react-native';

// // import SignUp from './screens/signup';
// import { SignUp } from './screens/signup';
// import Login from './screens/login';
// import OTPVerification from './screens/OTPVerification';
// import SignUpSuccess from './screens/OTPSuccessful';

// const Stack = createStackNavigator();

// export default function App() {
//   return (
//     <SafeAreaView style={styles.container}>
//       <NavigationContainer>
//         <Stack.Navigator>
//           <Stack.Screen name="SignUp" component={SignUp} />
//           <Stack.Screen name="Login" component={Login} />
//           <Stack.Screen name="OTPVerification" component={OTPVerification} />
//           <Stack.Screen name="OTPSuccessful" component={SignUpSuccess} />

//         </Stack.Navigator>
//       </NavigationContainer>
//       <StatusBar style="auto" />
//     </SafeAreaView>
//   );
// }

// const styles = StyleSheet.create({
//   container: {
//     flex: 1,
//     backgroundColor: '#fff',
//   },
// });
// App.js

import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { StatusBar } from 'expo-status-bar';
import { StyleSheet, SafeAreaView } from 'react-native';

import SignUp from './screens/signup';
import Login from './screens/login';
import OTPVerification from './screens/OTPVerification';
import SignUpSuccess from './screens/OTPSuccessful';
import SignUpAlert from './components/SignUpAlert';
import LandingPage from './screens/landingPage';
import NewsDetailsPage from './screens/newsDetailsPage';
import Home from './screens/home';
import Announcement from './screens/announcement';
import CreateNews from './screens/createNews';
import Categories from './screens/categories';
import Setting from './screens/setting';
import UserNewsDetailsPage from './screens/userNewsDetails';
import Events from './screens/events';
import Clubs from './screens/clubs';
import Terms from './screens/termsndConditions';
import Profile from './screens/profile';

const Stack = createStackNavigator();

export default function App() {
  return (
    <SafeAreaView style={styles.container}>
      <NavigationContainer>
        <Stack.Navigator>
          {/* Main screens */}
          <Stack.Screen name="LandingPage" component={LandingPage} options={{ headerShown: false }} />
          <Stack.Screen name="NewsDetailsPage" component={NewsDetailsPage} options={{ headerShown: false }} />
          <Stack.Screen name="SignUp" component={SignUp} options={{ headerShown: false }} />
          <Stack.Screen name="Login" component={Login} options={{ headerShown: false }} />
          <Stack.Screen name="OTPVerification" component={OTPVerification} options={{ headerShown: false }} />
          <Stack.Screen name="OTPSuccessful" component={SignUpSuccess} options={{ headerShown: false }} />

          {/* Add SignUpAlert screen */}
          <Stack.Screen name="SignUpAlert" component={SignUpAlert} options={{ headerShown: false }} />
          <Stack.Screen name="Home" component={Home} options={{ headerShown: false }} />
          <Stack.Screen name="Announcement" component={Announcement} options={{ headerShown: false }} />
          <Stack.Screen name="CreateNews" component={CreateNews} options={{ headerShown: false }} />
          <Stack.Screen name="Categories" component={Categories} options={{ headerShown: false }} />
          <Stack.Screen name="Setting" component={Setting} options={{ headerShown: false }} />
          <Stack.Screen name="UserNewsDetails" component={UserNewsDetailsPage} options={{ headerShown: false }} />
          <Stack.Screen name="Events" component={Events} options={{ headerShown: false }} />
          <Stack.Screen name="Clubs" component={Clubs} options={{ headerShown: false }} />
          <Stack.Screen name="Terms" component={Terms} options={{ headerShown: false }} />
          <Stack.Screen name="Profile" component={Profile} options={{ headerShown: false }} />

        </Stack.Navigator>
      </NavigationContainer>
      <StatusBar style="auto" />
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
});
