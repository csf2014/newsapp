import React from 'react';
import { StyleSheet, Text, View, TouchableOpacity, SafeAreaView } from 'react-native';
import { useNavigation } from '@react-navigation/native';

export const SignUpSuccess = () => {
  const navigation = useNavigation();

  const handleDone = () => {
    // Redirect user to the login page
    navigation.navigate('Login');
  };

  return (
    <SafeAreaView style={styles.container}>
      <View style={styles.innerContainer}>
        <Text style={styles.text}>You are successfully registered.</Text>
        <TouchableOpacity onPress={handleDone} style={styles.button}>
          <Text style={styles.buttonText}>Done</Text>
        </TouchableOpacity>
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  innerContainer: {
    width: '80%',
    borderWidth: 1,
    borderColor: "#42506B",
    borderRadius: 20,
    padding: 20,
    alignItems: 'center',
  },
  text: {
    textAlign: 'center',
    color: '#42506B',
    fontWeight: 'bold',
    fontSize: 20,
    marginBottom: 20,
  },
  button: {
    height: 40,
    backgroundColor: "#42506B",
    borderRadius: 20,
    justifyContent: 'center',
    alignItems: 'center',
    width: '50%',
  },
  buttonText: {
    color: 'white',
    fontSize: 16,
  },
});

export default SignUpSuccess;