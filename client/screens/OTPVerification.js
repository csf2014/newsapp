// // // // // import React, { useState, useEffect } from "react";
// // // // // import { StyleSheet, TextInput, Text, View, TouchableOpacity, SafeAreaView } from "react-native";
// // // // // import { useNavigation } from '@react-navigation/native';
// // // // // import SignUpSuccess from './OTPSuccessful'; // Import the OTPVerification component

// // // // // export const OTPVerification = ({ email }) => {
// // // // //   const navigation = useNavigation();
// // // // //   const [otp, setOTP] = useState(Array(6).fill(""));
// // // // //   const [otpError, setOTPError] = useState("");
// // // // //   const [showAlert, setShowAlert] = useState(true); // State to control visibility of the alert box

// // // // //   useEffect(() => {
// // // // //     // Hide the alert box after 5 seconds
// // // // //     const timer = setTimeout(() => {
// // // // //       setShowAlert(false);
// // // // //     }, 5000);

// // // // //     return () => clearTimeout(timer);
// // // // //   }, []); // Run only once on component mount

// // // // //   const verifyOTP = () => {
// // // // //     // Validate OTP
// // // // //     const enteredOTP = otp.join("");
// // // // //     if (!enteredOTP.trim()) {
// // // // //       setOTPError("Please enter the OTP.");
// // // // //       return;
// // // // //     }

// // // // //     // Call backend API to verify OTP
// // // // //     // For demonstration purposes, let's assume the API call is successful
// // // // //     const isValidOTP = true; // Replace this with the actual response from your backend

// // // // //     if (!isValidOTP) {
// // // // //       setOTPError("Invalid OTP. Please try again.");
// // // // //       return;
// // // // //     }

// // // // //     // If OTP is valid, navigate to the next screen (e.g., Home screen)
// // // // //     navigation.navigate('OTPSuccessful'); // Replace 'Login' with the name of your login screen
// // // // //   };

// // // // //   const handleResendOTP = () => {
// // // // //     // Logic to resend OTP
// // // // //     // For demonstration purposes, let's assume OTP is resent
// // // // //     console.log("Resend OTP");
// // // // //   };

// // // // //   const handleOTPChange = (value, index) => {
// // // // //     if (isNaN(value)) return;
// // // // //     const updatedOTP = [...otp];
// // // // //     updatedOTP[index] = value;
// // // // //     setOTP(updatedOTP);
// // // // //   };

// // // // //   return (
// // // // //     <SafeAreaView style={styles.container}>
// // // // //       {/* Alert box */}
// // // // //       {showAlert && (
// // // // //         <View style={styles.alertBox}>
// // // // //           <Text style={styles.alertText}>A OTP is sent to your email. Do not share it with anyone.</Text>
// // // // //         </View>
// // // // //       )}

// // // // //       <View style={styles.innerContainer}>
// // // // //         <Text style={styles.text}>OTP Verification</Text>
// // // // //         <View style={styles.otpContainer}>
// // // // //           {otp.map((digit, index) => (
// // // // //             <TextInput
// // // // //               key={index}
// // // // //               style={styles.otpInput}
// // // // //               value={digit}
// // // // //               maxLength={1}
// // // // //               keyboardType="numeric"
// // // // //               onChangeText={(text) => handleOTPChange(text, index)}
// // // // //             />
// // // // //           ))}
// // // // //         </View>
// // // // //         {otpError ? <Text style={styles.errorText}>{otpError}</Text> : null}
// // // // //         <TouchableOpacity onPress={verifyOTP} style={styles.button}>
// // // // //           <Text style={styles.buttonText}>Verify</Text>
// // // // //         </TouchableOpacity>
// // // // //         <Text style={styles.textt}>Do not share this code to anyone</Text>
// // // // //         <TouchableOpacity onPress={handleResendOTP} style={styles.resendButton}>
// // // // //           <Text style={styles.resendButtonText}>Send Again</Text>
// // // // //         </TouchableOpacity>
// // // // //       </View>
// // // // //     </SafeAreaView>
// // // // //   );
// // // // // };

// // // // // const styles = StyleSheet.create({
// // // // //   container: {
// // // // //     flex: 1,
// // // // //     backgroundColor: '#fff',
// // // // //     alignItems: 'center',
// // // // //     justifyContent: 'center',
// // // // //   },
// // // // //   innerContainer: {
// // // // //     width: '80%',
// // // // //     borderWidth: 1,
// // // // //     borderColor: "#42506B",
// // // // //     borderRadius: 20,
// // // // //     padding: 20,
// // // // //   },
// // // // //   text: {
// // // // //     textAlign: 'center',
// // // // //     color: '#42506B',
// // // // //     fontWeight: 'bold',
// // // // //     fontSize: 18,
// // // // //     marginBottom: 20,
// // // // //   },
// // // // //   otpContainer: {
// // // // //     flexDirection: 'row',
// // // // //     justifyContent: 'space-between',
// // // // //   },
// // // // //   otpInput: {
// // // // //     height: 40,
// // // // //     width: 35,
// // // // //     backgroundColor: "#fff",
// // // // //     borderWidth: 1,
// // // // //     borderColor: "#42506B",
// // // // //     borderRadius: 20,
// // // // //     paddingHorizontal: 10,
// // // // //     textAlign: 'center',
// // // // //   },
// // // // //   button: {
// // // // //     height: 40,
// // // // //     marginTop: 20,
// // // // //     backgroundColor: "#42506B",
// // // // //     borderRadius: 20,
// // // // //     justifyContent: 'center',
// // // // //     alignItems: 'center',
// // // // //   },
// // // // //   buttonText: {
// // // // //     color: 'white',
// // // // //     fontSize: 16,
// // // // //   },
// // // // //   textt: {
// // // // //     marginTop: 12,
// // // // //     textAlign: 'center',
// // // // //     color: '#42506B',
// // // // //     fontSize: 12,
// // // // //   },
// // // // //   resendButton: {
// // // // //     marginTop: 10,
// // // // //     alignItems: 'center',
// // // // //   },
// // // // //   resendButtonText: {
// // // // //     color: '#42506B',
// // // // //     fontWeight: 'bold',
// // // // //     fontSize: 13,
// // // // //   },
// // // // //   errorText: {
// // // // //     color: 'red',
// // // // //     marginTop: 10,
// // // // //     fontSize: 12,
// // // // //   },
// // // // //   alertBox: {
// // // // //     position: 'absolute',
// // // // //     top: 0,
// // // // //     left: 0,
// // // // //     right: 0,
// // // // //     backgroundColor: 'green',
// // // // //     paddingVertical: 20,
// // // // //     paddingHorizontal: 20,
// // // // //     zIndex: 1,
// // // // //   },
// // // // //   alertText: {
// // // // //     textAlign: 'center',
// // // // //     color: '#fff',
// // // // //     fontSize: 14,
// // // // //   },
// // // // // });

// // // // // export default OTPVerification;
// // // // import React, { useState, useEffect } from "react";
// // // // import { StyleSheet, TextInput, Text, View, TouchableOpacity, SafeAreaView } from "react-native";
// // // // import { useNavigation } from '@react-navigation/native';
// // // // import SignUpSuccess from './OTPSuccessful'; // Import the OTPVerification component

// // // // export const OTPVerification = ({ email }) => {
// // // //   const navigation = useNavigation();
// // // //   const [otp, setOTP] = useState(Array(6).fill(""));
// // // //   const [otpError, setOTPError] = useState("");
// // // //   const [showAlert, setShowAlert] = useState(true); // State to control visibility of the alert box

// // // //   useEffect(() => {
// // // //     // Hide the alert box after 5 seconds
// // // //     const timer = setTimeout(() => {
// // // //       setShowAlert(false);
// // // //     }, 5000);

// // // //     return () => clearTimeout(timer);
// // // //   }, []); // Run only once on component mount


// // // //   const verifyOTP = async () => {
// // // //     try {
// // // //       // Validate OTP
// // // //       const enteredOTP = otp.join("");
// // // //       if (!enteredOTP.trim()) {
// // // //         setOTPError("Please enter the OTP.");
// // // //         return;
// // // //       }

// // // //       // Call backend API to verify OTP
// // // //       const response = await fetch('http://10.9.92.25:3000/api/users/verify-otp', {
// // // //         method: 'POST',
// // // //         headers: {
// // // //           'Content-Type': 'application/json',
// // // //         },
// // // //         body: JSON.stringify({ otp: enteredOTP }), // Pass OTP as JSON
// // // //       });

// // // //       if (response.ok) {
// // // //         const data = await response.json();
// // // //         console.log('OTP verification successful:', data);
// // // //         // Navigate to the next screen (e.g., SignUpSuccess screen)
// // // //         navigation.navigate('SignUpSuccess');
// // // //       } else {
// // // //         const errorData = await response.json();
// // // //         console.error('OTP verification failed:', errorData);
// // // //         setOTPError("Invalid OTP. Please try again.");
// // // //       }
// // // //     } catch (error) {
// // // //       console.error('Error verifying OTP:', error);
// // // //       setOTPError("Error verifying OTP. Please try again.");
// // // //     }
// // // //   };

// // // //   const handleResendOTP = () => {
// // // //     // Logic to resend OTP
// // // //     // For demonstration purposes, let's assume OTP is resent
// // // //     console.log("Resend OTP");
// // // //   };

// // // //   const handleOTPChange = (value, index) => {
// // // //     if (isNaN(value)) return;
// // // //     const updatedOTP = [...otp];
// // // //     updatedOTP[index] = value;
// // // //     setOTP(updatedOTP);
// // // //   };

// // // //   return (
// // // //     <SafeAreaView style={styles.container}>
// // // //       {/* Alert box */}
// // // //       {showAlert && (
// // // //         <View style={styles.alertBox}>
// // // //           <Text style={styles.alertText}>A OTP is sent to your email. Do not share it with anyone.</Text>
// // // //         </View>
// // // //       )}

// // // //       <View style={styles.innerContainer}>
// // // //         <Text style={styles.text}>OTP Verification</Text>
// // // //         <View style={styles.otpContainer}>
// // // //           {otp.map((digit, index) => (
// // // //             <TextInput
// // // //               key={index}
// // // //               style={styles.otpInput}
// // // //               value={digit}
// // // //               maxLength={1}
// // // //               keyboardType="numeric"
// // // //               onChangeText={(text) => handleOTPChange(text, index)}
// // // //             />
// // // //           ))}
// // // //         </View>
// // // //         {otpError ? <Text style={styles.errorText}>{otpError}</Text> : null}
// // // //         {/* <TouchableOpacity onPress={verifyOTP} style={styles.button}>
// // // //           <Text style={styles.buttonText}>Verify</Text>
// // // //         </TouchableOpacity> */}
// // // //         <TouchableOpacity onPress={() => verifyOTP(email)} style={styles.button}>
// // // //           <Text style={styles.buttonText}>Verify</Text>
// // // //         </TouchableOpacity>

// // // //         <Text style={styles.textt}>Do not share this code to anyone</Text>
// // // //         <TouchableOpacity onPress={handleResendOTP} style={styles.resendButton}>
// // // //           <Text style={styles.resendButtonText}>Send Again</Text>
// // // //         </TouchableOpacity>
// // // //       </View>
// // // //     </SafeAreaView>
// // // //   );
// // // // };

// // // // const styles = StyleSheet.create({
// // // //   container: {
// // // //     flex: 1,
// // // //     backgroundColor: '#fff',
// // // //     alignItems: 'center',
// // // //     justifyContent: 'center',
// // // //   },
// // // //   innerContainer: {
// // // //     width: '80%',
// // // //     borderWidth: 1,
// // // //     borderColor: "#42506B",
// // // //     borderRadius: 20,
// // // //     padding: 20,
// // // //   },
// // // //   text: {
// // // //     textAlign: 'center',
// // // //     color: '#42506B',
// // // //     fontWeight: 'bold',
// // // //     fontSize: 18,
// // // //     marginBottom: 20,
// // // //   },
// // // //   otpContainer: {
// // // //     flexDirection: 'row',
// // // //     justifyContent: 'space-between',
// // // //   },
// // // //   otpInput: {
// // // //     height: 40,
// // // //     width: 35,
// // // //     backgroundColor: "#fff",
// // // //     borderWidth: 1,
// // // //     borderColor: "#42506B",
// // // //     borderRadius: 20,
// // // //     paddingHorizontal: 10,
// // // //     textAlign: 'center',
// // // //   },
// // // //   button: {
// // // //     height: 40,
// // // //     marginTop: 20,
// // // //     backgroundColor: "#42506B",
// // // //     borderRadius: 20,
// // // //     justifyContent: 'center',
// // // //     alignItems: 'center',
// // // //   },
// // // //   buttonText: {
// // // //     color: 'white',
// // // //     fontSize: 16,
// // // //   },
// // // //   textt: {
// // // //     marginTop: 12,
// // // //     textAlign: 'center',
// // // //     color: '#42506B',
// // // //     fontSize: 12,
// // // //   },
// // // //   resendButton: {
// // // //     marginTop: 10,
// // // //     alignItems: 'center',
// // // //   },
// // // //   resendButtonText: {
// // // //     color: '#42506B',
// // // //     fontWeight: 'bold',
// // // //     fontSize: 13,
// // // //   },
// // // //   errorText: {
// // // //     color: 'red',
// // // //     marginTop: 10,
// // // //     fontSize: 12,
// // // //   },
// // // //   alertBox: {
// // // //     position: 'absolute',
// // // //     top: 0,
// // // //     left: 0,
// // // //     right: 0,
// // // //     backgroundColor: 'green',
// // // //     paddingVertical: 20,
// // // //     paddingHorizontal: 20,
// // // //     zIndex: 1,
// // // //   },
// // // //   alertText: {
// // // //     textAlign: 'center',
// // // //     color: '#fff',
// // // //     fontSize: 14,
// // // //   },
// // // // });

// // // // export default OTPVerification;
// // // import React, { useState, useEffect } from "react";
// // // import { StyleSheet, TextInput, Text, View, TouchableOpacity, SafeAreaView } from "react-native";
// // // import { useNavigation } from '@react-navigation/native';
// // // import SignUpSuccess from './OTPSuccessful'; // Import the OTPVerification component

// // // export const OTPVerification = ({ email }) => {
// // //   const navigation = useNavigation();
// // //   const [otp, setOTP] = useState(Array(6).fill(""));
// // //   const [otpError, setOTPError] = useState("");
// // //   const [showAlert, setShowAlert] = useState(true); // State to control visibility of the alert box

// // //   useEffect(() => {
// // //     // Hide the alert box after 5 seconds
// // //     const timer = setTimeout(() => {
// // //       setShowAlert(false);
// // //     }, 5000);

// // //     return () => clearTimeout(timer);
// // //   }, []); // Run only once on component mount

// // //   // const verifyOTP = async () => {
// // //   //   try {
// // //   //     // Validate OTP
// // //   //     const enteredOTP = otp.join("");
// // //   //     if (!enteredOTP.trim()) {
// // //   //       setOTPError("Please enter the OTP.");
// // //   //       return;
// // //   //     }

// // //   //     // Call backend API to verify OTP
// // //   //     const response = await fetch('http://10.9.92.25:3000/api/users/verifyOTP', {
// // //   //       method: 'POST',
// // //   //       headers: {
// // //   //         'Content-Type': 'application/json',
// // //   //       },
// // //   //       body: JSON.stringify({ otp: enteredOTP }), // Pass OTP as JSON
// // //   //     });

// // //   //     if (response.ok) {
// // //   //       const data = await response.json();
// // //   //       console.log('OTP verification successful:', data);
// // //   //       // Navigate to the next screen (e.g., SignUpSuccess screen)
// // //   //       navigation.navigate('SignUpSuccess');
// // //   //     } else {
// // //   //       const errorData = await response.json();
// // //   //       console.error('OTP verification failed:', errorData);
// // //   //       setOTPError("Invalid OTP. Please try again.");
// // //   //     }
// // //   //   } catch (error) {
// // //   //     console.error('Error verifying OTP:', error);
// // //   //     setOTPError("Error verifying OTP. Please try again.");
// // //   //   }
// // //   // };

// // //   const verifyOTP = async () => {
// // //     try {
// // //       // Validate OTP
// // //       const enteredOTP = otp.join("");
// // //       if (!enteredOTP.trim()) {
// // //         setOTPError("Please enter the OTP.");
// // //         return;
// // //       }

// // //       // Call backend API to verify OTP
// // //       const response = await fetch('http://10.9.92.25:3000/api/users/verifyOTP', {
// // //         method: 'POST',
// // //         headers: {
// // //           'Content-Type': 'application/json',
// // //         },
// // //         body: JSON.stringify({ email: email, otp: enteredOTP }), // Pass email and OTP as JSON
// // //       });

// // //       if (response.ok) {
// // //         const data = await response.json();
// // //         console.log('OTP verification successful:', data);
// // //         // Navigate to the next screen (e.g., SignUpSuccess screen)
// // //         navigation.navigate('SignUpSuccess');
// // //       } else {
// // //         const errorData = await response.json();
// // //         console.error('OTP verification failed:', errorData);
// // //         setOTPError("Invalid OTP. Please try again.");
// // //       }
// // //     } catch (error) {
// // //       console.error('Error verifying OTP:', error);
// // //       setOTPError("Error verifying OTP. Please try again.");
// // //     }
// // //   };

// // //   const handleResendOTP = () => {
// // //     // Logic to resend OTP
// // //     // For demonstration purposes, let's assume OTP is resent
// // //     console.log("Resend OTP");
// // //   };

// // //   const handleOTPChange = (value, index) => {
// // //     if (isNaN(value)) return;
// // //     const updatedOTP = [...otp];
// // //     updatedOTP[index] = value;
// // //     setOTP(updatedOTP);
// // //   };

// // //   return (
// // //     <SafeAreaView style={styles.container}>
// // //       {/* Alert box */}
// // //       {showAlert && (
// // //         <View style={styles.alertBox}>
// // //           <Text style={styles.alertText}>A OTP is sent to your email. Do not share it with anyone.</Text>
// // //         </View>
// // //       )}

// // //       <View style={styles.innerContainer}>
// // //         <Text style={styles.text}>OTP Verification</Text>
// // //         <View style={styles.otpContainer}>
// // //           {otp.map((digit, index) => (
// // //             <TextInput
// // //               key={index}
// // //               style={styles.otpInput}
// // //               value={digit}
// // //               maxLength={1}
// // //               keyboardType="numeric"
// // //               onChangeText={(text) => handleOTPChange(text, index)}
// // //             />
// // //           ))}
// // //         </View>
// // //         {otpError ? <Text style={styles.errorText}>{otpError}</Text> : null}
// // //         <TouchableOpacity onPress={verifyOTP} style={styles.button}>
// // //           <Text style={styles.buttonText}>Verify</Text>
// // //         </TouchableOpacity>

// // //         <Text style={styles.textt}>Do not share this code to anyone</Text>
// // //         <TouchableOpacity onPress={handleResendOTP} style={styles.resendButton}>
// // //           <Text style={styles.resendButtonText}>Send Again</Text>
// // //         </TouchableOpacity>
// // //       </View>
// // //     </SafeAreaView>
// // //   );
// // // };

// // // const styles = StyleSheet.create({
// // //   container: {
// // //     flex: 1,
// // //     backgroundColor: '#fff',
// // //     alignItems: 'center',
// // //     justifyContent: 'center',
// // //   },
// // //   innerContainer: {
// // //     width: '80%',
// // //     borderWidth: 1,
// // //     borderColor: "#42506B",
// // //     borderRadius: 20,
// // //     padding: 20,
// // //   },
// // //   text: {
// // //     textAlign: 'center',
// // //     color: '#42506B',
// // //     fontWeight: 'bold',
// // //     fontSize: 18,
// // //     marginBottom: 20,
// // //   },
// // //   otpContainer: {
// // //     flexDirection: 'row',
// // //     justifyContent: 'space-between',
// // //   },
// // //   otpInput: {
// // //     height: 40,
// // //     width: 35,
// // //     backgroundColor: "#fff",
// // //     borderWidth: 1,
// // //     borderColor: "#42506B",
// // //     borderRadius: 20,
// // //     paddingHorizontal: 10,
// // //     textAlign: 'center',
// // //   },
// // //   button: {
// // //     height: 40,
// // //     marginTop: 20,
// // //     backgroundColor: "#42506B",
// // //     borderRadius: 20,
// // //     justifyContent: 'center',
// // //     alignItems: 'center',
// // //   },
// // //   buttonText: {
// // //     color: 'white',
// // //     fontSize: 16,
// // //   },
// // //   textt: {
// // //     marginTop: 12,
// // //     textAlign: 'center',
// // //     color: '#42506B',
// // //     fontSize: 12,
// // //   },
// // //   resendButton: {
// // //     marginTop: 10,
// // //     alignItems: 'center',
// // //   },
// // //   resendButtonText: {
// // //     color: '#42506B',
// // //     fontWeight: 'bold',
// // //     fontSize: 13,
// // //   },
// // //   errorText: {
// // //     color: 'red',
// // //     marginTop: 10,
// // //     fontSize: 12,
// // //   },
// // //   alertBox: {
// // //     position: 'absolute',
// // //     top: 0,
// // //     left: 0,
// // //     right: 0,
// // //     backgroundColor: 'green',
// // //     paddingVertical: 20,
// // //     paddingHorizontal: 20,
// // //     zIndex: 1,
// // //   },
// // //   alertText: {
// // //     textAlign: 'center',
// // //     color: '#fff',
// // //     fontSize: 14,
// // //   },
// // // });

// // // export default OTPVerification;
// // // Existing imports...
// // import React, { useState, useEffect } from "react";
// // import { StyleSheet, TextInput, Text, View, TouchableOpacity, SafeAreaView } from "react-native";
// // import { useNavigation } from '@react-navigation/native';
// // import SignUpSuccess from './OTPSuccessful'; // Import the OTPVerification component

// // export const OTPVerification = ({ route }) => {
// //   const navigation = useNavigation();
// //   const [otp, setOTP] = useState(Array(6).fill(""));
// //   const [otpError, setOTPError] = useState("");
// //   const [showAlert, setShowAlert] = useState(true); // State to control visibility of the alert box
// //   const [email, setEmail] = useState('');

// //   // useEffect to hide the alert box after 5 seconds
// //   useEffect(() => {
// //     const timer = setTimeout(() => {
// //       setShowAlert(false);
// //     }, 5000);

// //     return () => clearTimeout(timer);
// //   }, []); // Run only once on component mount
// //   useEffect(() => {
// //     if (route.params && route.params.email) {
// //       setEmail(route.params.email);
// //     }
// //   }, [route.params]);

// //   const verifyOTP = async () => {

// //     try {
// //       const response = await fetch('http://10.9.92.25:3000/api/users/verify-otp', {
// //         method: 'POST',
// //         headers: {
// //           'Content-Type': 'application/json',
// //         },
// //         body: JSON.stringify({ email, otp: otp.join('') }),
// //       });
// //       console.log(otp)
// //       console.log(email)

// //       if (response.ok) {
// //         const data = await response.json();
// //         alert('OTP verified successfully');
// //         console.log("success");
// //         navigation.navigate('Login');
// //       } else {
// //         const errorData = await response.json();
// //         alert('OTP verification failed');
// //       }
// //     } catch (error) {
// //       console.log('Error during verification:', error);
// //     }
// //   };



// //   const handleResendOTP = () => {
// //     // Logic to resend OTP
// //     // For demonstration purposes, let's assume OTP is resent
// //     console.log("Resend OTP");
// //   };

// //   const handleOTPChange = (value, index) => {
// //     if (isNaN(value)) return;
// //     const updatedOTP = [...otp];
// //     updatedOTP[index] = value;
// //     setOTP(updatedOTP);
// //   };

// //   return (
// //     <SafeAreaView style={styles.container}>
// //       {/* Alert box */}
// //       {showAlert && (
// //         <View style={styles.alertBox}>
// //           <Text style={styles.alertText}>A OTP is sent to your email. Do not share it with anyone.</Text>
// //         </View>
// //       )}

// //       <View style={styles.innerContainer}>
// //         <Text style={styles.text}>OTP Verification</Text>
// //         <View style={styles.otpContainer}>
// //           {otp.map((digit, index) => (
// //             <TextInput
// //               key={index}
// //               style={styles.otpInput}
// //               value={digit}
// //               maxLength={1}
// //               keyboardType="numeric"
// //               onChangeText={(text) => handleOTPChange(text, index)}
// //             />
// //           ))}
// //         </View>
// //         {otpError ? <Text style={styles.errorText}>{otpError}</Text> : null}
// //         <TouchableOpacity onPress={verifyOTP} style={styles.button}>
// //           <Text style={styles.buttonText}>Verify</Text>
// //         </TouchableOpacity>

// //         <Text style={styles.textt}>Do not share this code to anyone</Text>
// //         <TouchableOpacity onPress={handleResendOTP} style={styles.resendButton}>
// //           <Text style={styles.resendButtonText}>Send Again</Text>
// //         </TouchableOpacity>
// //       </View>
// //     </SafeAreaView>
// //   );
// // }
// // const styles = StyleSheet.create({
// //   container: {
// //     flex: 1,
// //     backgroundColor: '#fff',
// //     alignItems: 'center',
// //     justifyContent: 'center',
// //   },
// //   innerContainer: {
// //     width: '80%',
// //     borderWidth: 1,
// //     borderColor: "#42506B",
// //     borderRadius: 20,
// //     padding: 20,
// //   },
// //   text: {
// //     textAlign: 'center',
// //     color: '#42506B',
// //     fontWeight: 'bold',
// //     fontSize: 18,
// //     marginBottom: 20,
// //   },
// //   otpContainer: {
// //     flexDirection: 'row',
// //     justifyContent: 'space-between',
// //   },
// //   otpInput: {
// //     height: 40,
// //     width: 35,
// //     backgroundColor: "#fff",
// //     borderWidth: 1,
// //     borderColor: "#42506B",
// //     borderRadius: 20,
// //     paddingHorizontal: 10,
// //     textAlign: 'center',
// //   },
// //   button: {
// //     height: 40,
// //     marginTop: 20,
// //     backgroundColor: "#42506B",
// //     borderRadius: 20,
// //     justifyContent: 'center',
// //     alignItems: 'center',
// //   },
// //   buttonText: {
// //     color: 'white',
// //     fontSize: 16,
// //   },
// //   textt: {
// //     marginTop: 12,
// //     textAlign: 'center',
// //     color: '#42506B',
// //     fontSize: 12,
// //   },
// //   resendButton: {
// //     marginTop: 10,
// //     alignItems: 'center',
// //   },
// //   resendButtonText: {
// //     color: '#42506B',
// //     fontWeight: 'bold',
// //     fontSize: 13,
// //   },
// //   errorText: {
// //     color: 'red',
// //     marginTop: 10,
// //     fontSize: 12,
// //   },
// //   alertBox: {
// //     position: 'absolute',
// //     top: 0,
// //     left: 0,
// //     right: 0,
// //     backgroundColor: 'green',
// //     paddingVertical: 20,
// //     paddingHorizontal: 20,
// //     zIndex: 1,
// //   },
// //   alertText: {
// //     textAlign: 'center',
// //     color: '#fff',
// //     fontSize: 14,
// //   },
// // });

// // export default OTPVerification;
// import React, { useState, useEffect } from "react";
// const { StyleSheet, TextInput, Text, View, TouchableOpacity, SafeAreaView } = require("react-native");
// const { useNavigation } = require('@react-navigation/native');

// export const OTPVerification = ({ route }) => {
//   const navigation = useNavigation();
//   const [otp, setOTP] = useState(Array(6).fill(""));
//   const [otpError, setOTPError] = useState("");
//   const [showAlert, setShowAlert] = useState(true); // State to control visibility of the alert box
//   const [email, setEmail] = useState('');

//   // useEffect to hide the alert box after 5 seconds
//   useEffect(() => {
//     const timer = setTimeout(() => {
//       setShowAlert(false);
//     }, 5000);

//     return () => clearTimeout(timer);
//   }, []); // Run only once on component mount
//   useEffect(() => {
//     if (route.params && route.params.email) {
//       setEmail(route.params.email);
//     }
//   }, [route.params]);

//   const verifyOTP = async () => {
//     try {
//       const response = await fetch('http://: 10.9.85.243:8080/api/users/verify-otp', {
//         method: 'POST',
//         headers: {
//           'Content-Type': 'application/json',
//         },
//         body: JSON.stringify({ email, otp: otp.join('') }),
//       });

//       if (response.ok) {
//         const data = await response.json();
//         alert('OTP verified successfully');
//         console.log("success");
//         navigation.navigate('Login');
//       } else {
//         const errorData = await response.json();
//         alert('OTP verification failed');
//       }
//     } catch (error) {
//       console.log('Error during verification:', error);
//     }
//   };

//   const resendOTP = async () => {
//     try {
//       const response = await fetch('http://10.9.85.243:8080/api/users/resendOTP', {
//         method: 'POST',
//         headers: {
//           'Content-Type': 'application/json',
//         },
//         body: JSON.stringify({ email }),
//       });

//       if (response.ok) {
//         const data = await response.json();
//         alert('New OTP sent successfully');
//       } else {
//         const errorData = await response.json();
//         alert('Failed to resend OTP');
//       }
//     } catch (error) {
//       console.log('Error during OTP resend:', error);
//     }
//   };

//   const handleOTPChange = (value, index) => {
//     if (isNaN(value)) return;
//     const updatedOTP = [...otp];
//     updatedOTP[index] = value;
//     setOTP(updatedOTP);
//   };

//   return (
//     <SafeAreaView style={styles.container}>
//       {/* Alert box */}
//       {showAlert && (
//         <View style={styles.alertBox}>
//           <Text style={styles.alertText}>A OTP is sent to your email. Do not share it with anyone.</Text>
//         </View>
//       )}

//       <View style={styles.innerContainer}>
//         <Text style={styles.text}>OTP Verification</Text>
//         <View style={styles.otpContainer}>
//           {otp.map((digit, index) => (
//             <TextInput
//               key={index}
//               style={styles.otpInput}
//               value={digit}
//               maxLength={1}
//               keyboardType="numeric"
//               onChangeText={(text) => handleOTPChange(text, index)}
//             />
//           ))}
//         </View>
//         {otpError ? <Text style={styles.errorText}>{otpError}</Text> : null}
//         <TouchableOpacity onPress={verifyOTP} style={styles.button}>
//           <Text style={styles.buttonText}>Verify</Text>
//         </TouchableOpacity>

//         <Text style={styles.textt}>Do not share this code to anyone</Text>
//         <TouchableOpacity onPress={resendOTP} style={styles.resendButton}>
//           <Text style={styles.resendButtonText}>Send Again</Text>
//         </TouchableOpacity>
//       </View>
//     </SafeAreaView>
//   );
// };

// const styles = StyleSheet.create({
//   container: {
//     flex: 1,
//     backgroundColor: '#fff',
//     alignItems: 'center',
//     justifyContent: 'center',
//   },
//   innerContainer: {
//     width: '80%',
//     borderWidth: 1,
//     borderColor: "#42506B",
//     borderRadius: 20,
//     padding: 20,
//   },
//   text: {
//     textAlign: 'center',
//     color: '#42506B',
//     fontWeight: 'bold',
//     fontSize: 18,
//     marginBottom: 20,
//   },
//   otpContainer: {
//     flexDirection: 'row',
//     justifyContent: 'space-between',
//   },
//   otpInput: {
//     height: 40,
//     width: 35,
//     backgroundColor: "#fff",
//     borderWidth: 1,
//     borderColor: "#42506B",
//     borderRadius: 20,
//     paddingHorizontal: 10,
//     textAlign: 'center',
//   },
//   button: {
//     height: 40,
//     marginTop: 20,
//     backgroundColor: "#42506B",
//     borderRadius: 20,
//     justifyContent: 'center',
//     alignItems: 'center',
//   },
//   buttonText: {
//     color: 'white',
//     fontSize: 16,
//   },
//   textt: {
//     marginTop: 12,
//     textAlign: 'center',
//     color: '#42506B',
//     fontSize: 12,
//   },
//   resendButton: {
//     marginTop: 10,
//     alignItems: 'center',
//   },
//   resendButtonText: {
//     color: '#42506B',
//     fontWeight: 'bold',
//     fontSize: 13,
//   },
//   errorText: {
//     color: 'red',
//     marginTop: 10,
//     fontSize: 12,
//   },
//   alertBox: {
//     position: 'absolute',
//     top: 0,
//     left: 0,
//     right: 0,
//     backgroundColor: 'green',
//     paddingVertical: 20,
//     paddingHorizontal: 20,
//     zIndex: 1,
//   },
//   alertText: {
//     textAlign: 'center',
//     color: '#fff',
//     fontSize: 14,
//   },
// });

// module.exports = OTPVerification;
import React, { useState, useEffect, useRef } from "react";
const { StyleSheet, TextInput, Text, View, TouchableOpacity, SafeAreaView } = require("react-native");
const { useNavigation } = require('@react-navigation/native');

export const OTPVerification = ({ route }) => {
  const navigation = useNavigation();
  const [otp, setOTP] = useState(Array(6).fill(""));
  const [otpError, setOTPError] = useState("");
  const [showAlert, setShowAlert] = useState(true); // State to control visibility of the alert box
  const [email, setEmail] = useState('');
  const otpRefs = useRef([]);

  // useEffect to hide the alert box after 5 seconds
  useEffect(() => {
    const timer = setTimeout(() => {
      setShowAlert(false);
    }, 5000);

    return () => clearTimeout(timer);
  }, []); // Run only once on component mount

  useEffect(() => {
    if (route.params && route.params.email) {
      setEmail(route.params.email);
    }
  }, [route.params]);

  const verifyOTP = async () => {
    try {
      const response = await fetch('http://10.9.85.243:8080/api/users/verify-otp', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({ email, otp: otp.join('') }),
      });

      if (response.ok) {
        const data = await response.json();
        alert('OTP verified successfully');
        console.log("success");
        navigation.navigate('Login');
      } else {
        const errorData = await response.json();
        alert('OTP verification failed');
      }
    } catch (error) {
      console.log('Error during verification:', error);
    }
  };

  const resendOTP = async () => {
    try {
      const response = await fetch('http://10.9.85.243:8080/api/users/resendOTP', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({ email }),
      });

      if (response.ok) {
        const data = await response.json();
        alert('New OTP sent successfully');
      } else {
        const errorData = await response.json();
        alert('Failed to resend OTP');
      }
    } catch (error) {
      console.log('Error during OTP resend:', error);
    }
  };

  const handleOTPChange = (value, index) => {
    if (isNaN(value)) return;
    const updatedOTP = [...otp];
    updatedOTP[index] = value;
    setOTP(updatedOTP);

    // Move to the next input field if the current input is not empty
    if (value !== "" && index < otp.length - 1) {
      otpRefs.current[index + 1].focus();
    }
  };

  return (
    <SafeAreaView style={styles.container}>
      {/* Alert box */}
      {showAlert && (
        <View style={styles.alertBox}>
          <Text style={styles.alertText}>A OTP is sent to your email. Do not share it with anyone.</Text>
        </View>
      )}

      <View style={styles.innerContainer}>
        <Text style={styles.text}>OTP Verification</Text>
        <View style={styles.otpContainer}>
          {otp.map((digit, index) => (
            <TextInput
              key={index}
              style={styles.otpInput}
              value={digit}
              maxLength={1}
              keyboardType="numeric"
              onChangeText={(text) => handleOTPChange(text, index)}
              ref={(el) => (otpRefs.current[index] = el)}
            />
          ))}
        </View>
        {otpError ? <Text style={styles.errorText}>{otpError}</Text> : null}
        <TouchableOpacity onPress={verifyOTP} style={styles.button}>
          <Text style={styles.buttonText}>Verify</Text>
        </TouchableOpacity>

        <Text style={styles.textt}>Do not share this code with anyone</Text>
        <TouchableOpacity onPress={resendOTP} style={styles.resendButton}>
          <Text style={styles.resendButtonText}>Send Again</Text>
        </TouchableOpacity>
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  innerContainer: {
    width: '80%',
    borderWidth: 1,
    borderColor: "#42506B",
    borderRadius: 20,
    padding: 20,
  },
  text: {
    textAlign: 'center',
    color: '#42506B',
    fontWeight: 'bold',
    fontSize: 18,
    marginBottom: 20,
  },
  otpContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  otpInput: {
    height: 40,
    width: 35,
    backgroundColor: "#fff",
    borderWidth: 1,
    borderColor: "#42506B",
    borderRadius: 20,
    paddingHorizontal: 10,
    textAlign: 'center',
  },
  button: {
    height: 40,
    marginTop: 20,
    backgroundColor: "#ffa500",
    borderRadius: 20,
    justifyContent: 'center',
    alignItems: 'center',
  },
  buttonText: {
    color: 'white',
    fontSize: 16,
  },
  textt: {
    marginTop: 12,
    textAlign: 'center',
    color: '#42506B',
    fontSize: 12,
  },
  resendButton: {
    marginTop: 10,
    alignItems: 'center',
  },
  resendButtonText: {
    color: '#42506B',
    fontWeight: 'bold',
    fontSize: 13,
  },
  errorText: {
    color: 'red',
    marginTop: 10,
    fontSize: 12,
  },
  alertBox: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    backgroundColor: 'green',
    paddingVertical: 20,
    paddingHorizontal: 20,
    zIndex: 1,
  },
  alertText: {
    textAlign: 'center',
    color: '#fff',
    fontSize: 14,
  },
});

module.exports = OTPVerification;
