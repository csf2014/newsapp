
import React, { useState, useEffect } from 'react';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { View, Text, Image, StyleSheet, TextInput, TouchableOpacity, ScrollView, Alert } from 'react-native';
import { FontAwesome } from '@expo/vector-icons';
import Header from '../components/Header';
import BottomTabNavigator from '../components/bottomTabNavigator';
import SignUpAlert from '../components/SignUpAlert';
import { useNavigation } from '@react-navigation/native';
// import FontAwesome from 'react-native-vector-icons/FontAwesome';

const NewsDetailsPage = ({ route }) => {
  const { newsId } = route.params;
  const navigation = useNavigation();
  const [newsDetails, setNewsDetails] = useState(null);
  const [comments, setComments] = useState([]);
  const [newComment, setNewComment] = useState('');
  const [showAllComments, setShowAllComments] = useState(false);

  useEffect(() => {
    //   const fetchNewsDetails = async () => {
    //     try {
    //       const response = await fetch(`http://10.9.92.25:8080/api/news/${newsId}`);
    //       if (!response.ok) {
    //         throw new Error('Failed to fetch news');
    //       }
    //       const data = await response.json();
    //       setNewsDetails(Array.isArray(data) ? data : [data]);
    //       console.log('Fetched news details:', data);

    //       const commentsResponse = await fetch(`http://10.9.92.25:8080/api/news/${newsId}/comments`);
    //       if (!commentsResponse.ok) {
    //         throw new Error('Failed to fetch comments');
    //       }
    //       const commentsData = await commentsResponse.json();
    //       setComments(commentsData.comments);
    //       console.log('Fetched comments:', commentsData.comments);
    //     } catch (error) {
    //       console.error('Error fetching data:', error);
    //     }
    //   };

    //   fetchNewsDetails();
    // }, [newsId]);
    //   const fetchNewsDetails = async () => {
    //     try {
    //       const response = await fetch(`http://10.9.85.243:8080/api/news/${newsId}`);
    //       if (!response.ok) {
    //         throw new Error('Failed to fetch news');
    //       }
    //       const data = await response.json();
    //       setNewsDetails(Array.isArray(data) ? data : [data]);
    //       console.log('Fetched news details:', data);

    //       const commentsResponse = await fetch(`http://10.9.85.243:8080/api/news/${newsId}/comments`);
    //       if (!commentsResponse.ok) {
    //         throw new Error('Failed to fetch comments');
    //       }
    //       const commentsData = await commentsResponse.json();
    //       setComments(commentsData.comments.reverse());
    //       console.log('Fetched comments:', commentsData.comments);

    //       // Check if this news is bookmarked by the current user
    //       const email = await AsyncStorage.getItem('email');
    //       // const bookmarkStatus = await AsyncStorage.getItem(`bookmark_${email}_${newsId}`);
    //       // setIsBookmarked(JSON.parse(bookmarkStatus) || false);
    //     } catch (error) {
    //       console.error('Error fetching data:', error);
    //     }
    //   };

    //   fetchNewsDetails();
    // }, [newsId]);
    const fetchNewsDetails = async () => {
      try {
        const response = await fetch(`http://10.9.85.243:8080/api/news/${newsId}`);
        if (!response.ok) {
          throw new Error('Failed to fetch news');
        }
        const data = await response.json();
        setNewsDetails(Array.isArray(data) ? data : [data]);
        console.log('Fetched news details:', data);

        const commentsResponse = await fetch(`http://10.9.85.243:8080/api/news/${newsId}/comments`);
        if (!commentsResponse.ok) {
          throw new Error('Failed to fetch comments');
        }
        const commentsData = await commentsResponse.json();
        setComments(commentsData.comments.reverse());
        console.log('Fetched comments:', commentsData.comments);
      } catch (error) {
        console.error('Error fetching data:', error);
      }
    };

    fetchNewsDetails();
  }, [newsId]);

  // const handleCommentSubmit = async () => {
  //   try {
  //     if (!newComment.trim()) {
  //       console.log('Comment cannot be empty');
  //       return;
  //     }

  //     const token = await AsyncStorage.getItem('token');
  //     const email = await AsyncStorage.getItem('email');
  //     console.log(token, email);

  //     if (!token) {
  //       console.log('User is not logged in');
  //       throw new Error('User is not logged in');
  //     }

  //     const response = await fetch(`http://10.9.92.25:8080/api/news/${newsId}/comment`, {
  //       method: 'POST',
  //       headers: {
  //         'Content-Type': 'application/json',
  //         'Authorization': `Bearer ${token}`,
  //       },
  //       body: JSON.stringify({ text: newComment, user: email }),
  //     });

  //     if (!response.ok) {
  //       const errorMessage = await response.text();
  //       throw new Error(`Failed to submit comment: ${errorMessage}`);
  //     }

  //     const newCommentData = await response.json();
  //     setComments([...comments, newCommentData]);
  //     setNewComment('');
  //     console.log('Comment successfully submitted:', newCommentData);
  //   } catch (error) {
  //     console.error('Error submitting comment:', error.message);
  //     navigation.navigate('SignUpAlert');
  //   }
  // };
  // Function to handle adding news to favorites
  const handleBookmark = () => {
    // Navigate to SignUpAlert page when Add Favorites is clicked
    navigation.navigate('SignUpAlert');
  };
  // Function to handle posting comment
  const handlePostComment = () => {
    // Navigate to SignUpAlert page when post comment is clicked
    navigation.navigate('SignUpAlert');
  };
  // Function to report a comment
  const handleReportComment = () => {
    navigation.navigate('SignUpAlert');
  };
  const handleShowMoreComments = () => {
    setShowAllComments(!showAllComments);
  };

  if (!newsDetails) {
    return (
      <View style={styles.container}>
        <Text>Loading...</Text>
      </View>
    );
  }

  const displayedComments = showAllComments ? comments : comments.slice(0, 4);

  return (
    <View style={styles.container}>
      <Header />
      <ScrollView contentContainerStyle={styles.contentContainer}>
        <TouchableOpacity onPress={() => navigation.goBack()} style={styles.backButton}>
          <FontAwesome name="chevron-left" size={20} color="#ffa500" />
        </TouchableOpacity>
        <View style={styles.newsImageContainer}>
          {newsDetails[0].news.image ? (
            <Image
              source={{ uri: `http://10.9.85.243:8080/${newsDetails[0].news.image.replace(/\\/g, '/')}` }}
              style={styles.newsImage}
              onLoad={() => console.log('Image loaded successfully')}
              onError={(error) => console.error('Error loading image:', error)}
            />
          ) : (
            <View style={styles.placeholderImage}>
              <Text style={styles.placeholderText}>Image Not Available</Text>
            </View>
          )}
        </View>
        <Text style={styles.newsTitle}>{newsDetails[0].news.title}</Text>

        <View style={styles.reporterContainer}>
          <Text style={styles.reporter}>Reported by {newsDetails[0].news.reporter}</Text>
          {/* <TouchableOpacity onPress={SignUpAlert}>
            <FontAwesome name="bookmark" size={20} color="#ffa500" />
          </TouchableOpacity> */}
          <TouchableOpacity onPress={handleBookmark}>
            <FontAwesome name="bookmark" size={20} color="#ccc" />
          </TouchableOpacity>
        </View>

        <Text style={styles.newsDetails}>{newsDetails[0].news.content}</Text>

        {/* <View style={styles.commentInputContainer}>
          <TextInput
            style={styles.commentInput}
            placeholder="Leave a comment"
            value={newComment}
            onChangeText={setNewComment}
          />
          <TouchableOpacity onPress={handleCommentSubmit}>
            <FontAwesome name="send" size={18} color="grey" />
          </TouchableOpacity>
        </View> */}
        <View style={styles.commentInputContainer}>
          <TextInput
            style={styles.commentInput}
            placeholder="Leave a comment"
            onSubmitEditing={handlePostComment}
          />
          <TouchableOpacity onPress={handlePostComment}>
            <FontAwesome name="send" size={18} color="grey" marginRight={20} />
          </TouchableOpacity>
        </View>
        <Text style={styles.readMoreComments}>Comments</Text>
        {/* {displayedComments.length > 0 ? (
          displayedComments.map((comment) => (
            <View key={comment._id} style={styles.commentItem}>
              <Image source={{ uri: comment.userImage || 'default_user_image_uri' }} style={styles.userImage} />
              <View style={styles.commentTextContainer}>
                <View style={styles.userInfo}>
                  <Text style={styles.commentAuthor}>{comment.user}</Text>
                  <Text style={styles.commentTimestamp}>{new Date(comment.createdAt).toLocaleString()}</Text>
                </View>
                <Text style={styles.commentContent}>{comment.text}</Text>
              </View>
              <TouchableOpacity onPress={() => handleReportComment(comment._id)}>
                <Text style={styles.reportButton}>Report</Text>
              </TouchableOpacity>
            </View>
          ))
        ) : (
          <Text style={styles.noCommentsText}>No comments available</Text>
        )}
      </ScrollView>
      <Text style={styles.readMoreComments} onPress={handleShowMoreComments}>
        {showAllComments ? 'Show less comments' : 'Read more comments'}
      </Text> */}
        {displayedComments.length > 0 ? (
          displayedComments.map((comment) => (
            <View key={comment._id} style={styles.commentContainer}>
              <Image source={{ uri: `http://10.9.85.243:8080${comment.user.profileImage}` }} style={styles.commentProfileImage} />
              <View style={styles.commentContent}>
                <View style={styles.commentHeader}>
                  <Text style={styles.commentName}>{comment.user.name}</Text>
                  <Text style={styles.commentTimestamp}>
                    {new Date(comment.createdAt).toLocaleString('en-GB', {
                      day: '2-digit',
                      month: '2-digit',
                      year: 'numeric',
                      hour: '2-digit',
                      minute: '2-digit',
                    })}
                  </Text>
                  <TouchableOpacity onPress={() => handleReportComment(comment._id)}>
                    <Text style={styles.reportComment}>Report</Text>
                  </TouchableOpacity>
                </View>
                <Text style={styles.commentText}>{comment.text}</Text>
              </View>
            </View>
          ))
        ) : (
          <Text style={styles.noCommentsText}>No comments available.</Text>
        )}

        {comments.length > 4 && (
          <TouchableOpacity onPress={handleShowMoreComments}>
            <Text style={styles.showMoreComments}>
              {showAllComments ? 'Show Less' : 'Show More'}
            </Text>
          </TouchableOpacity>
        )}
      </ScrollView>
      <BottomTabNavigator navigation={navigation} />
    </View>
  );
};


const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  contentContainer: {
    padding: 16,
  },
  newsImageContainer: {
    alignItems: 'center',
    marginBottom: 16,
  },
  newsImage: {
    width: '100%',
    height: 200,
    resizeMode: 'cover',
  },
  placeholderImage: {
    width: '100%',
    height: 200,
    backgroundColor: '#ccc',
    justifyContent: 'center',
    alignItems: 'center',
  },
  placeholderText: {
    color: '#fff',
    fontSize: 16,
  },
  newsTitle: {
    fontSize: 24,
    fontWeight: 'bold',
    marginBottom: 8,
  },
  reporterContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginBottom: 16,
  },
  reporter: {
    fontSize: 16,
    color: '#777',
  },
  newsDetails: {
    fontSize: 16,
    lineHeight: 24,
    marginBottom: 16,
  },
  commentInputContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 16,
    borderWidth: 1,
    borderColor: '#ddd',
    borderRadius: 8,
    padding: 8,
  },
  commentInput: {
    flex: 1,
    marginRight: 8,
  },
  readMoreComments: {
    fontSize: 16,
    fontWeight: 'bold',
    marginBottom: 8,
  },
  commentContainer: {
    flexDirection: 'row',
    alignItems: 'flex-start',
    marginBottom: 16,
  },
  commentProfileImage: {
    width: 40,
    height: 40,
    borderRadius: 20,
    marginRight: 8,
  },
  commentContent: {
    flex: 1,
  },
  commentHeader: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  commentName: {
    fontWeight: 'bold',
    fontSize: 14,
    flex: 1,
  },
  commentText: {
    marginBottom: 4,
    fontSize: 14,
    color: '#333',
  },
  commentTimestamp: {
    fontSize: 12,
    color: '#777',
    textAlign: 'center',
  },
  reportComment: {
    color: 'red',
    fontSize: 12,
    marginLeft: 8,
  },
  noCommentsText: {
    textAlign: 'center',
    marginVertical: 16,
    color: '#777',
  },
  showMoreComments: {
    textAlign: 'center',
    color: '#007bff',
    marginTop: 8,
  },

  backButton: {
    marginTop: 5, // Add margin above the button
    marginBottom: 5, // Add margin below the button
  },

});


export default NewsDetailsPage;
