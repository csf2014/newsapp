import React from 'react';
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import { FontAwesome } from '@expo/vector-icons'; // Import FontAwesome icon library
import Header from '../components/Header';
import UserBottomTabNavigator from '../components/userBottomTabNavigator'; // Adjust the path if necessary

const Categories = () => {
  const navigation = useNavigation();

  // Function to navigate to the Events page
  const navigateToEvents = () => {
    navigation.navigate('Events');
  };

  // Function to navigate to the Clubs page
  const navigateToClubs = () => {
    navigation.navigate('Clubs');
  };

  return (
    <View style={styles.container}>
      <Header />

      <View style={styles.content}>
        <TouchableOpacity style={styles.button} onPress={navigateToEvents}>
          <Text style={styles.buttonText}>Events</Text>
          <FontAwesome name="chevron-right" size={18} color="#ffa500" />
        </TouchableOpacity>
        <TouchableOpacity style={styles.button} onPress={navigateToClubs}>
          <Text style={styles.buttonText}>Clubs</Text>
          <FontAwesome name="chevron-right" size={18} color="#ffa500" />
        </TouchableOpacity>
      </View>

      <UserBottomTabNavigator navigation={navigation} />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  content: {
    flex: 1,
  },
  button: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingVertical: 15,
    paddingHorizontal: 20,
    borderColor: '#aaa',
    borderBottomWidth: 1,
  },
  buttonText: {
    color: '#444',
    fontSize: 18,
  },
});

export default Categories;
