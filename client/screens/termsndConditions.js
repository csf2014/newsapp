import React from 'react';
import { View, Text, ScrollView, StyleSheet, TouchableOpacity } from 'react-native';
import { FontAwesome } from '@expo/vector-icons'; // Import FontAwesome icon library
import Header from '../components/Header';
import UserBottomTabNavigator from '../components/userBottomTabNavigator'; // Adjust the path if necessary
import { useNavigation } from '@react-navigation/native'; // Import useNavigation hook

const Terms = () => {
  const navigation = useNavigation(); // Access navigation object

  return (
    <View style={styles.container}>
      <Header />
      <ScrollView contentContainerStyle={styles.contentContainer}>
        {/* Back Button */}
        <View style={styles.headerWithText}>
          <TouchableOpacity onPress={() => navigation.goBack()} style={styles.leftArrowContainer}>
            <FontAwesome name="chevron-left" size={20} color="#ffa500" />
          </TouchableOpacity>
          <Text style={styles.headerText}>Terms and Conditions</Text>
        </View>        
        <Text style={styles.text1}>
          Terms and Conditions for GCIT NEWS.
        </Text>
        <Text style={styles.text}>
          Welcome to GCIT NEWS! By using our app, you agree to these Terms and Conditions.
        </Text>
        <Text style={styles.text}>
          The content provided in GCIT NEWS is for informational purposes only.
        </Text>
        <Text style={styles.text}>
          We strive to ensure the accuracy of the information, but we cannot guarantee it.
        </Text>
        <Text style={styles.text}>
          You may use the content for personal, non-commercial purposes only.
        </Text>
        <Text style={styles.text}>
          Reproduction, distribution, or modification of our content without permission is prohibited.
        </Text>
        <Text style={styles.text}>
          We reserve the right to modify or discontinue the app at any time without prior notice.
        </Text>
        <Text style={styles.text}>
          Your use of GCIT NEWS constitutes acceptance of any future updates to these Terms and Conditions.
        </Text>
        <Text style={styles.text}>
          By using GCIT NEWS, you agree to these terms. Enjoy staying informed with our app!
        </Text>
        {/* Add more terms and conditions text as needed */}
      </ScrollView>
      <UserBottomTabNavigator navigation={navigation}/>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  contentContainer: {
    flexGrow: 1,
  },
    headerWithText: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-start',
        padding: 20,
    },
    headerText: {
        fontSize: 18,
        fontWeight: 'bold',
        color: '#333',
        left: 10,

    },
    text1: {
        paddingHorizontal: 20,
        fontSize: 16,
        marginBottom: 16,
        top: 20,
        textAlign: 'center',
      },
  text: {
    paddingHorizontal: 20,
    fontSize: 14,
    marginBottom: 20,
    top: 30,
    textAlign: 'center',

  },
});

export default Terms;
