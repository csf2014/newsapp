
// // // import React, { useState, useEffect } from 'react';
// // // import { View, Image, FlatList, StyleSheet, Text, TouchableOpacity, ScrollView } from 'react-native';
// // // import { useNavigation } from '@react-navigation/native';
// // // import Header from '../components/Header';
// // // import SearchBarMenu from '../components/searchBarMenu';
// // // import BottomTabNavigator from '../components/bottomTabNavigator';

// // // const LandingPage = () => {
// // //   const navigation = useNavigation();
// // //   const [news, setNews] = useState([]);
// // //   const [filteredNewsData, setFilteredNewsData] = useState([]);

// // //   useEffect(() => {
// // //     const fetchApprovedNews = async () => {
// // //       try {
// // //         const response = await fetch("http://10.9.92.25:8080/api/news/approved-news");
// // //         if (!response.ok) {
// // //           throw new Error("Failed to fetch news");
// // //         }
// // //         const data = await response.json();
// // //         const approvedNews = Array.isArray(data.news) ? data.news : [data.news];
// // //         setNews(approvedNews);
// // //         setFilteredNewsData(approvedNews);
// // //       } catch (error) {
// // //         console.error("Error fetching news:", error);
// // //       }
// // //     };

// // //     fetchApprovedNews();
// // //   }, []);


// // //   const filterNewsData = (text) => {
// // //     const filteredData = news.filter(item => item.title.toLowerCase().includes(text.toLowerCase()));
// // //     setFilteredNewsData(filteredData);
// // //   };

// // //   const navigateToNewsDetails = (id) => {
// // //     navigation.navigate('NewsDetails', { newsId: id });
// // //   };

// // //   const renderNewsItem = ({ item, index }) => {
// // //     return (
// // //       <TouchableOpacity key={item._id} onPress={() => navigateToNewsDetails(item._id)}>
// // //         <View style={[styles.newsItem, index % 2 !== 0 && styles.newsItemReverse]}>
// // //           {/* <View style={styles.newsImageContainer}>
// // //             {item.image ? (
// // //               <Image
// // //                 source={{ uri: `http://10.9.92.25:8080/${item.image}` }}
// // //                 style={styles.newsImage}
// // //                 onLoad={() => console.log('Image loaded successfully')}
// // //                 onError={(error) => console.error('Error loading image:', error)}
// // //               />
// // //             ) : (
// // //               <View style={styles.placeholderImage}>
// // //                 <Text style={styles.placeholderText}>Image Not Available</Text>
// // //               </View>
// // //             )}

// // //           </View> */}
// // //           <View style={styles.newsImageContainer}>
// // //             {item.image ? (
// // //               <Image
// // //                 source={{ uri: `http://10.9.92.25:8080/${item.image.replace(/\\/g, '/')}` }}
// // //                 style={styles.newsImage}
// // //                 onLoad={() => console.log('Image loaded successfully')}
// // //                 onError={(error) => console.error('Error loading image:', error)}
// // //               />
// // //             ) : (
// // //               <View style={styles.placeholderImage}>
// // //                 <Text style={styles.placeholderText}>Image Not Available</Text>
// // //               </View>
// // //             )}
// // //           </View>

// // //           <View style={styles.newsDetails}>
// // //             <Text style={styles.newsTitle}>{item.title}</Text>
// // //             <Text style={styles.newsDescription}>{item.content}</Text>
// // //           </View>
// // //         </View>
// // //       </TouchableOpacity>
// // //     );
// // //   };

// // //   const navigateToSignup = () => {
// // //     navigation.navigate('SignUp');
// // //   };

// // //   return (
// // //     <View style={styles.container}>
// // //       <Header />
// // //       <SearchBarMenu placeholder="Search by news title..." onSearch={filterNewsData} />
// // //       <ScrollView contentContainerStyle={styles.scrollViewContent}>
// // //         <View style={styles.content}>
// // //           <View style={styles.headerWithText}>
// // //             <Image source={require('../assets/header.jpg')} style={styles.headerBackground} />
// // //             <Text style={styles.headerText}>Learn What's Happening Across Our College</Text>
// // //           </View>
// // //           <View style={styles.signupRow}>
// // //             <View style={styles.leftSide}>
// // //               <Text style={styles.signupText}>Join us today and stay updated!</Text>
// // //             </View>
// // //             <TouchableOpacity style={styles.rightSide} onPress={navigateToSignup}>
// // //               <Text style={styles.signupButton}>Sign Up</Text>
// // //             </TouchableOpacity>
// // //           </View>
// // //           <FlatList
// // //             data={filteredNewsData}
// // //             renderItem={renderNewsItem}
// // //             keyExtractor={item => item._id}
// // //           />
// // //         </View>
// // //       </ScrollView>
// // //       <BottomTabNavigator navigation={navigation} />
// // //     </View>
// // //   );
// // // };

// // // const styles = StyleSheet.create({
// // //   scrollViewContent: {
// // //     flexGrow: 1,
// // //   },
// // //   container: {
// // //     flex: 1,
// // //     backgroundColor: '#fff',
// // //   },
// // //   content: {
// // //     flex: 1,
// // //   },
// // //   headerWithText: {
// // //     textAlign: "center",
// // //     justifyContent: 'center',
// // //     paddingHorizontal: 20,
// // //     marginBottom: 20,
// // //   },
// // //   headerBackground: {
// // //     width: '100%',
// // //     height: 200,
// // //     resizeMode: 'cover',
// // //     borderRadius: 10,
// // //     marginBottom: 10,
// // //     opacity: 1,
// // //   },
// // //   headerText: {
// // //     fontSize: 24,
// // //     fontWeight: 'bold',
// // //     color: '#fff',
// // //     position: 'absolute',
// // //     bottom: 20,
// // //     top: 70,
// // //     left: 55,
// // //     textAlign: "center",
// // //     justifyContent: 'center',
// // //   },
// // //   signupRow: {
// // //     flexDirection: 'row',
// // //     paddingHorizontal: 20,
// // //     marginBottom: 20,
// // //   },
// // //   leftSide: {
// // //     flex: 2,
// // //   },
// // //   rightSide: {
// // //     flex: 1,
// // //     height: 40,
// // //     width: 20,
// // //     alignItems: 'center',
// // //     justifyContent: 'center',
// // //     borderRadius: 50,
// // //     backgroundColor: '#ffa500',
// // //   },
// // //   signupText: {
// // //     fontSize: 20,
// // //   },
// // //   signupButton: {
// // //     fontSize: 15,
// // //     color: '#fff',
// // //     fontWeight: 'bold',
// // //     paddingVertical: 10,
// // //     paddingHorizontal: 10,
// // //   },
// // //   newsItem: {
// // //     flexDirection: 'row',
// // //     alignItems: 'center',
// // //     paddingHorizontal: 20,
// // //     height: 150,
// // //   },
// // //   newsItemReverse: {
// // //     flexDirection: 'row-reverse',
// // //   },
// // //   newsImageContainer: {
// // //     flex: 1,
// // //   },
// // //   newsImage: {
// // //     width: '100%',
// // //     height: '100%',
// // //   },
// // //   newsDetails: {
// // //     flex: 1,
// // //   },
// // //   newsTitle: {
// // //     paddingHorizontal: 15,
// // //     paddingTop: 5,
// // //     fontSize: 13,
// // //     fontWeight: 'bold',
// // //   },
// // //   newsDescription: {
// // //     paddingHorizontal: 15,
// // //     paddingTop: 5,
// // //     fontSize: 11,
// // //     color: '#555',
// // //   },
// // //   placeholderImage: {
// // //     width: '100%',
// // //     height: '100%',
// // //     backgroundColor: '#eee',
// // //     alignItems: 'center',
// // //     justifyContent: 'center',
// // //   },
// // //   placeholderText: {
// // //     fontSize: 16,
// // //     color: '#999',
// // //   },
// // // });

// // // export default LandingPage;
// // import React, { useState, useEffect } from 'react';
// // import { View, Image, FlatList, StyleSheet, Text, TouchableOpacity, ScrollView } from 'react-native';
// // import { useNavigation } from '@react-navigation/native';
// // import Header from '../components/Header';
// // import SearchBarMenu from '../components/searchBarMenu';
// // import BottomTabNavigator from '../components/bottomTabNavigator';

// // const LandingPage = () => {
// //   const navigation = useNavigation();
// //   const [news, setNews] = useState([]);
// //   const [filteredNewsData, setFilteredNewsData] = useState([]);

// //   useEffect(() => {
// //     const fetchApprovedNews = async () => {
// //       try {
// //         const response = await fetch("http://10.9.92.25:8080/api/news/approved-news");
// //         if (!response.ok) {
// //           throw new Error("Failed to fetch news");
// //         }
// //         const data = await response.json();
// //         const approvedNews = Array.isArray(data.news) ? data.news : [data.news];

// //         // Sort the news array by upload date in descending order
// //         approvedNews.sort((a, b) => new Date(b.uploadDate) - new Date(a.uploadDate));

// //         setNews(approvedNews);
// //         setFilteredNewsData(approvedNews);
// //       } catch (error) {
// //         console.error("Error fetching news:", error);
// //       }
// //     };

// //     fetchApprovedNews();
// //   }, []);


// //   const filterNewsData = (text) => {
// //     const filteredData = news.filter(item => item.title.toLowerCase().includes(text.toLowerCase()));
// //     setFilteredNewsData(filteredData);
// //   };

// //   const navigateToNewsDetails = (id) => {
// //     navigation.navigate('NewsDetails', { newsId: id });
// //   };

// //   const renderNewsItem = ({ item, index }) => {
// //     return (
// //       <TouchableOpacity key={item._id} onPress={() => navigateToNewsDetails(item._id)}>
// //         <View style={[styles.newsItem, index % 2 !== 0 && styles.newsItemReverse]}>
// //           <View style={styles.newsImageContainer}>
// //             {item.image ? (
// //               <Image
// //                 source={{ uri: `http://10.9.92.25:8080/${item.image.replace(/\\/g, '/')}` }}
// //                 style={styles.newsImage}
// //                 onLoad={() => console.log('Image loaded successfully')}
// //                 onError={(error) => console.error('Error loading image:', error)}
// //               />
// //             ) : (
// //               <View style={styles.placeholderImage}>
// //                 <Text style={styles.placeholderText}>Image Not Available</Text>
// //               </View>
// //             )}
// //           </View>
// //           <View style={styles.newsDetails}>
// //             <Text style={styles.newsTitle}>{item.title}</Text>
// //             <Text style={styles.newsDescription}>{item.content}</Text>
// //           </View>
// //         </View>
// //       </TouchableOpacity>
// //     );
// //   };

// //   const navigateToSignup = () => {
// //     navigation.navigate('SignUp');
// //   };
// // //
// //   return (
// //     <View style={styles.container}>
// //       <Header />
// //       <SearchBarMenu placeholder="Search by news title..." onSearch={filterNewsData} />
// //       <ScrollView contentContainerStyle={styles.scrollViewContent}>
// //         <View style={styles.content}>
// //           <View style={styles.headerWithText}>
// //             <Image source={require('../assets/header.jpg')} style={styles.headerBackground} />
// //             <Text style={styles.headerText}>Learn What's Happening Across Our College</Text>
// //           </View>
// //           <View style={styles.signupRow}>
// //             <View style={styles.leftSide}>
// //               <Text style={styles.signupText}>Join us today and stay updated!</Text>
// //             </View>
// //             <TouchableOpacity style={styles.rightSide} onPress={navigateToSignup}>
// //               <Text style={styles.signupButton}>Sign Up</Text>
// //             </TouchableOpacity>
// //           </View>
// //           <FlatList
// //             data={filteredNewsData}
// //             renderItem={renderNewsItem}
// //             keyExtractor={item => item._id}
// //           />
// //         </View>
// //       </ScrollView>
// //       <BottomTabNavigator navigation={navigation} />
// //     </View>
// //   );
// // };

// // const styles = StyleSheet.create({
// //   scrollViewContent: {
// //     flexGrow: 1,
// //   },
// //   container: {
// //     flex: 1,
// //     backgroundColor: '#fff',
// //   },
// //   content: {
// //     flex: 1,
// //   },
// //   headerWithText: {
// //     textAlign: "center",
// //     justifyContent: 'center',
// //     paddingHorizontal: 20,
// //     marginBottom: 20,
// //   },
// //   headerBackground: {
// //     width: '100%',
// //     height: 200,
// //     resizeMode: 'cover',
// //     borderRadius: 10,
// //     marginBottom: 10,
// //     opacity: 1,
// //   },
// //   headerText: {
// //     fontSize: 24,
// //     fontWeight: 'bold',
// //     color: '#fff',
// //     position: 'absolute',
// //     bottom: 20,
// //     top: 70,
// //     left: 55,
// //     textAlign: "center",
// //     justifyContent: 'center',
// //   },
// //   signupRow: {
// //     flexDirection: 'row',
// //     paddingHorizontal: 20,
// //     marginBottom: 20,
// //   },
// //   leftSide: {
// //     flex: 2,
// //   },
// //   rightSide: {
// //     flex: 1,
// //     height: 40,
// //     width: 20,
// //     alignItems: 'center',
// //     justifyContent: 'center',
// //     borderRadius: 50,
// //     backgroundColor: '#ffa500',
// //   },
// //   signupText: {
// //     fontSize: 20,
// //   },
// //   signupButton: {
// //     fontSize: 15,
// //     color: '#fff',
// //     fontWeight: 'bold',
// //     paddingVertical: 10,
// //     paddingHorizontal: 10,
// //   },
// //   newsItem: {
// //     flexDirection: 'row',
// //     alignItems: 'center',
// //     paddingHorizontal: 20,
// //     height: 150,
// //   },
// //   newsItemReverse: {
// //     flexDirection: 'row-reverse',
// //   },
// //   newsImageContainer: {
// //     flex: 1,
// //   },
// //   newsImage: {
// //     width: '100%',
// //     height: '100%',
// //   },
// //   newsDetails: {
// //     flex: 1,
// //   },
// //   newsTitle: {
// //     paddingHorizontal: 15,
// //     paddingTop: 5,
// //     fontSize: 13,
// //     fontWeight: 'bold',
// //   },
// //   newsDescription: {
// //     paddingHorizontal: 15,
// //     paddingTop: 5,
// //     fontSize: 11,
// //     color: '#555',
// //   },
// //   placeholderImage: {
// //     width: '100%',
// //     height: '100%',
// //     backgroundColor: '#eee',
// //     alignItems: 'center',
// //     justifyContent: 'center',
// //   },
// //   placeholderText: {
// //     fontSize: 16,
// //     color: '#999',
// //   },
// // });

// // export default LandingPage;
// import React, { useState, useEffect } from 'react';
// import { View, Image, FlatList, StyleSheet, Text, TouchableOpacity, ScrollView } from 'react-native';
// import { useNavigation } from '@react-navigation/native';
// import Header from '../components/Header';
// import SearchBarMenu from '../components/searchBarMenu';
// import BottomTabNavigator from '../components/bottomTabNavigator';
// import SignUp from '../screens/signup';
// import NewsDetailsPage from '../screens/newsDetailsPage'

// const LandingPage = () => {
//   const navigation = useNavigation();
//   const [news, setNews] = useState([]);
//   const [filteredNewsData, setFilteredNewsData] = useState([]);
//   const [searchKeyword, setSearchKeyword] = useState('');

//   useEffect(() => {
//     const fetchApprovedNews = async () => {
//       try {
//         const response = await fetch("http://10.9.92.25:8080/api/news/approved-news");
//         if (!response.ok) {
//           throw new Error("Failed to fetch news");
//         }
//         const data = await response.json();
//         const approvedNews = Array.isArray(data.news) ? data.news : [data.news];

//         // Sort the news array by upload date in descending order
//         approvedNews.sort((a, b) => new Date(b.uploadDate) - new Date(a.uploadDate));

//         setNews(approvedNews);
//         setFilteredNewsData(approvedNews);
//       } catch (error) {
//         console.error("Error fetching news:", error);
//       }
//     };

//     fetchApprovedNews();
//   }, []);

//   const filterNewsData = async (text) => {
//     try {
//       setSearchKeyword(text);
//       const response = await fetch(`http://10.9.92.25:8080/api/news/search?keyword=${text}`);
//       if (!response.ok) {
//         throw new Error("Failed to fetch filtered news");
//       }
//       const data = await response.json();
//       const filteredNews = Array.isArray(data.news) ? data.news : [data.news];
//       setFilteredNewsData(filteredNews);
//     } catch (error) {
//       console.error("Error fetching filtered news:", error);
//     }
//   };

//   const navigateToNewsDetails = (id) => {
//     navigation.navigate('NewsDetailsPage', { newsId: id });
//   };
//   // const navigateToNewsDetails = (id) => {
//   //   navigation.navigate('NewsDetails', { newsId: id });
//   // };

//   const renderNewsItem = ({ item, index }) => {
//     return (
//       <TouchableOpacity key={item._id} onPress={() => navigateToNewsDetails(item._id)}>
//         <View style={[styles.newsItem, index % 2 !== 0 && styles.newsItemReverse]}>
//           <View style={styles.newsImageContainer}>
//             {item.image ? (
//               <Image
//                 source={{ uri: `http://10.9.92.25:8080/${item.image.replace(/\\/g, '/')}` }}
//                 style={styles.newsImage}
//                 onLoad={() => console.log('Image loaded successfully')}
//                 onError={(error) => console.error('Error loading image:', error)}
//               />
//             ) : (
//               <View style={styles.placeholderImage}>
//                 <Text style={styles.placeholderText}>Image Not Available</Text>
//               </View>
//             )}
//           </View>
//           <View style={styles.newsDetails}>
//             <Text style={styles.newsTitle}>{item.title}</Text>
//             <Text style={styles.newsDescription}>{item.content}</Text>
//           </View>
//         </View>
//       </TouchableOpacity>
//     );
//   };

//   const navigateToSignup = () => {
//     navigation.navigate(SignUp);
//   };

//   return (
//     <View style={styles.container}>
//       <Header />
//       <SearchBarMenu placeholder="Search by news title..." onSearch={filterNewsData} />
//       <ScrollView contentContainerStyle={styles.scrollViewContent}>
//         <View style={styles.content}>
//           <View style={styles.headerWithText}>
//             <Image source={require('../assets/header.jpg')} style={styles.headerBackground} />
//             <Text style={styles.headerText}>Learn What's Happening Across Our College</Text>
//           </View>
//           <View style={styles.signupRow}>
//             <View style={styles.leftSide}>
//               <Text style={styles.signupText}>Join us today and stay updated!</Text>
//             </View>
//             <TouchableOpacity style={styles.rightSide} onPress={navigateToSignup}>
//               <Text style={styles.signupButton}>Sign Up</Text>
//             </TouchableOpacity>
//           </View>
//           <FlatList
//             data={filteredNewsData}
//             renderItem={renderNewsItem}
//             keyExtractor={item => item._id}
//           />
//         </View>
//       </ScrollView>
//       <BottomTabNavigator navigation={navigation} />
//     </View>
//   );
// };

// const styles = StyleSheet.create({
//   scrollViewContent: {
//     flexGrow: 1,
//   },
//   container: {
//     flex: 1,
//     backgroundColor: '#fff',
//   },
//   content: {
//     flex: 1,
//   },
//   headerWithText: {
//     textAlign: "center",
//     justifyContent: 'center',
//     paddingHorizontal: 20,
//     marginBottom: 20,
//   },
//   headerBackground: {
//     width: '100%',
//     height: 200,
//     resizeMode: 'cover',
//     borderRadius: 10,
//     marginBottom: 10,
//     opacity: 1,
//   },
//   headerText: {
//     fontSize: 24,
//     fontWeight: 'bold',
//     color: '#fff',
//     position: 'absolute',
//     bottom: 20,
//     top: 70,
//     left: 55,
//     textAlign: "center",
//     justifyContent: 'center',
//   },
//   signupRow: {
//     flexDirection: 'row',
//     paddingHorizontal: 20,
//     marginBottom: 20,
//   },
//   leftSide: {
//     flex: 2,
//   },
//   rightSide: {
//     flex: 1,
//     height: 40,
//     width: 20,
//     alignItems: 'center',
//     justifyContent: 'center',
//     borderRadius: 50,
//     backgroundColor: '#ffa500',
//   },
//   signupText: {
//     fontSize: 20,
//   },
//   signupButton: {
//     fontSize: 15,
//     color: '#fff',
//     fontWeight: 'bold',
//     paddingVertical: 10,
//     paddingHorizontal: 10,
//   },
//   newsItem: {
//     flexDirection: 'row',
//     alignItems: 'center',
//     paddingHorizontal: 20,
//     height: 150,
//   },
//   newsItemReverse: {
//     flexDirection: 'row-reverse',
//   },
//   newsImageContainer: {
//     flex: 1,
//   },
//   newsImage: {
//     width: '100%',
//     height: '100%',
//   },
//   newsDetails: {
//     flex: 1,
//   },
//   newsTitle: {
//     paddingHorizontal: 15,
//     paddingTop: 5,
//     fontSize: 13,
//     fontWeight: 'bold',
//   },
//   newsDescription: {
//     paddingHorizontal: 15,
//     paddingTop: 5,
//     fontSize: 11,
//     color: '#555',
//   },
//   placeholderImage: {
//     width: '100%',
//     height: '100%',
//     backgroundColor: '#eee',
//     alignItems: 'center',
//     justifyContent: 'center',
//   },
//   placeholderText: {
//     fontSize: 16,
//     color: '#999',
//   },
// });

// export default LandingPage;
import React, { useState, useEffect } from 'react';
import { View, Image, FlatList, StyleSheet, Text, TouchableOpacity, ScrollView } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import Header from '../components/Header';
import SearchBarMenu from '../components/searchBarMenu';
import BottomTabNavigator from '../components/bottomTabNavigator'; // Adjust import path if necessary
import SignUp from '../screens/signup';
import NewsDetailsPage from '../screens/newsDetailsPage';

const LandingPage = () => {
  const navigation = useNavigation();
  const [news, setNews] = useState([]);
  const [filteredNewsData, setFilteredNewsData] = useState([]);
  const [searchKeyword, setSearchKeyword] = useState('');

  useEffect(() => {
    const fetchApprovedNews = async () => {
      try {
        const response = await fetch("http://10.9.85.243:8080/api/news/approved-news");
        if (!response.ok) {
          throw new Error("Failed to fetch news");
        }
        const data = await response.json();
        const approvedNews = Array.isArray(data.news) ? data.news : [data.news];

        // Sort the news array by upload date in descending order
        approvedNews.sort((a, b) => new Date(b.uploadDate) - new Date(a.uploadDate));

        setNews(approvedNews);
        setFilteredNewsData(approvedNews);
      } catch (error) {
        console.error("Error fetching news:", error);
      }
    };

    fetchApprovedNews();
  }, []);

  const filterNewsData = async (text) => {
    try {
      setSearchKeyword(text);
      const response = await fetch(`http://10.9.85.243:8080/api/news/search?keyword=${text}`);
      if (!response.ok) {
        throw new Error("Failed to fetch filtered news");
      }
      const data = await response.json();
      const filteredNews = Array.isArray(data.news) ? data.news : [data.news];
      setFilteredNewsData(filteredNews);
    } catch (error) {
      console.error("Error fetching filtered news:", error);
    }
  };

  // const navigateToNewsDetails = (id) => {
  //   navigation.navigate('NewsDetailsPage', { newsId: id });
  //   console.log(news)
  // };
  const navigateToNewsDetails = (id) => {
    console.log("Navigating to news details with id:", id);
    navigation.navigate('NewsDetailsPage', { newsId: id });
  };

  const renderNewsItem = ({ item, index }) => {
    return (
      <TouchableOpacity key={item._id} onPress={() => navigateToNewsDetails(item._id)}>
        <View style={[styles.newsItem, index % 2 !== 0 && styles.newsItemReverse]}>
          <View style={styles.newsImageContainer}>
            {item.image ? (
              <Image
                // source={{ uri: `http://10.9.92.25:8080/${item.image.replace(/\\/g, '/')}` }}
                source={{ uri: `http://10.9.85.243:8080/${item.image.replace(/\\/g, '/')}` }}
                style={styles.newsImage}
                onLoad={() => console.log('Image loaded successfully')}
                onError={(error) => console.error('Error loading image:', error)}
              />
            ) : (
              <View style={styles.placeholderImage}>
                <Text style={styles.placeholderText}>Image Not Available</Text>
              </View>
            )}
          </View>
          <View style={styles.newsDetails}>
            <Text style={styles.newsTitle}>{item.title}</Text>
            {/* <Text style={styles.newsDescription}>{item.content}</Text> */}
            <Text style={styles.newsDescription}>
              {item.content ? item.content.substring(0, 150) : 'No content available'}...
              {/* {item.content ? item.news.content.substring(0, 150) : 'No content available'}... */}
            </Text>
          </View>
        </View>
      </TouchableOpacity>
    );
  };

  const navigateToSignup = () => {
    navigation.navigate(SignUp);
  };

  return (
    <View style={styles.container}>
      <Header />
      <SearchBarMenu placeholder="Search by news title..." onSearch={filterNewsData} />
      <ScrollView contentContainerStyle={styles.scrollViewContent}>
        <View style={styles.content}>
          <View style={styles.headerWithText}>
            <Image source={require('../assets/header.jpg')} style={styles.headerBackground} />
            <Text style={styles.headerText}>Learn What's Happening Across Our College</Text>
          </View>
          <View style={styles.signupRow}>
            <View style={styles.leftSide}>
              <Text style={styles.signupText}>Join us today and stay updated!</Text>
            </View>
            <TouchableOpacity style={styles.rightSide} onPress={navigateToSignup}>
              <Text style={styles.signupButton}>Sign Up</Text>
            </TouchableOpacity>
          </View>
          <FlatList
            data={filteredNewsData}
            renderItem={renderNewsItem}
            keyExtractor={item => item._id}
          />
        </View>
      </ScrollView>
      <BottomTabNavigator navigation={navigation} />
    </View>
  );
};

const styles = StyleSheet.create({
  scrollViewContent: {
    flexGrow: 1,
  },
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  content: {
    flex: 1,
  },
  headerWithText: {
    textAlign: "center",
    justifyContent: 'center',
    paddingHorizontal: 20,
    marginBottom: 20,
  },
  headerBackground: {
    width: '100%',
    height: 200,
    resizeMode: 'cover',
    borderRadius: 10,
    marginBottom: 10,
    opacity: 1,
  },
  headerText: {
    fontSize: 24,
    fontWeight: 'bold',
    color: '#fff',
    position: 'absolute',
    bottom: 20,
    top: 70,
    left: 55,
    textAlign: "center",
    justifyContent: 'center',
  },
  signupRow: {
    flexDirection: 'row',
    paddingHorizontal: 20,
    marginBottom: 20,
  },
  leftSide: {
    flex: 2,
  },
  rightSide: {
    flex: 1,
    height: 40,
    width: 20,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 50,
    backgroundColor: '#ffa500',
  },
  signupText: {
    fontSize: 20,
  },
  signupButton: {
    fontSize: 15,
    color: '#fff',
    fontWeight: 'bold',
    paddingVertical: 10,
    paddingHorizontal: 10,
  },
  newsItem: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 20,
    height: 150,
  },
  newsItemReverse: {
    flexDirection: 'row-reverse',
  },
  newsImageContainer: {
    flex: 1,
  },
  newsImage: {
    width: '100%',
    height: '100%',
  },
  newsDetails: {
    flex: 1,
  },
  newsTitle: {
    paddingHorizontal: 15,
    paddingTop: 5,
    fontSize: 13,
    fontWeight: 'bold',
  },
  newsDescription: {
    paddingHorizontal: 15,
    paddingTop: 5,
    fontSize: 11,
    color: '#555',
  },
  placeholderImage: {
    width: '100%',
    height: '100%',
    backgroundColor: '#eee',
    alignItems: 'center',
    justifyContent: 'center',
  },
  placeholderText: {
    fontSize: 16,
    color: '#999',
  },
});

export default LandingPage;
