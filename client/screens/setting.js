// import React from 'react';
// import { View, Text, StyleSheet, TouchableOpacity } from 'react-native';
// import { useNavigation } from '@react-navigation/native';
// import { FontAwesome } from '@expo/vector-icons'; // Import FontAwesome icon library
// import Header from '../components/Header';
// import UserBottomTabNavigator from '../components/userBottomTabNavigator'; // Adjust the path if necessary

// const Setting = () => {
//   const navigation = useNavigation();

//   // Function to navigate to the Events page
//   const navigateToProfile = () => {
//     navigation.navigate('Profile');
//   };

//   // Function to navigate to the Clubs page
//   const navigateToTerms = () => {
//     navigation.navigate('Terms');
//   };

//   const navigateToLogout = () => {
//     navigation.navigate('LandingPage');
//   };

//   return (
//     <View style={styles.container}>
//       <Header />

//       <View style={styles.content}>
//         <TouchableOpacity style={styles.button} onPress={navigateToProfile}>
//           <Text style={styles.buttonText}>Profile</Text>
//           <FontAwesome name="chevron-right" size={18} color="#ffa500" />
//         </TouchableOpacity>
//         <TouchableOpacity style={styles.button} onPress={navigateToTerms}>
//           <Text style={styles.buttonText}>Terms and Conditions</Text>
//           <FontAwesome name="chevron-right" size={18} color="#ffa500" />
//         </TouchableOpacity>
//         <TouchableOpacity style={styles.button} onPress={navigateToLogout}>
//           <Text style={styles.buttonText}>Log out</Text>
//           <FontAwesome name="chevron-right" size={18} color="#ffa500" />
//         </TouchableOpacity>
//       </View>

//       <UserBottomTabNavigator navigation={navigation} />
//     </View>
//   );
// };

// const styles = StyleSheet.create({
//   container: {
//     flex: 1,
//   },
//   content: {
//     flex: 1,
//   },
//   button: {
//     flexDirection: 'row',
//     justifyContent: 'space-between',
//     alignItems: 'center',
//     paddingVertical: 15,
//     paddingHorizontal: 20,
//     borderColor: '#aaa',
//     borderBottomWidth: 1,
//   },
//   buttonText: {
//     color: '#444',
//     fontSize: 16,
//   },
// });

// export default Setting;
import React from 'react';
import { View, Text, StyleSheet, TouchableOpacity, Alert } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import { FontAwesome } from '@expo/vector-icons'; // Import FontAwesome icon library
import Header from '../components/Header';
import UserBottomTabNavigator from '../components/userBottomTabNavigator'; // Adjust the path if necessary

const Setting = () => {
  const navigation = useNavigation();

  // Function to navigate to the Profile page
  const navigateToProfile = () => {
    navigation.navigate('Profile');
  };

  // Function to navigate to the Terms and Conditions page
  const navigateToTerms = () => {
    navigation.navigate('Terms');
  };

  // Function to handle logout
  const navigateToLogout = async () => {
    try {
      const response = await fetch('http://10.9.85.243:8080/api/users/logout', {
        method: 'POST', // or 'GET' depending on your API
        headers: {
          'Content-Type': 'application/json',
          // Include any additional headers if required, like Authorization tokens
        },
      });

      if (response.ok) {
        // If logout is successful, navigate to the LandingPage
        Alert.alert('Logout Successful');
        navigation.navigate('LandingPage');

      } else {
        // Handle errors if logout is not successful
        const errorData = await response.json();
        Alert.alert('Logout Failed', errorData.message || 'Something went wrong, please try again.');
      }
    } catch (error) {
      // Handle network or other errors
      Alert.alert('Error', 'Unable to logout. Please check your network connection.');
    }
  };

  return (
    <View style={styles.container}>
      <Header />

      <View style={styles.content}>
        <TouchableOpacity style={styles.button} onPress={navigateToProfile}>
          <Text style={styles.buttonText}>Profile</Text>
          <FontAwesome name="chevron-right" size={18} color="#ffa500" />
        </TouchableOpacity>
        <TouchableOpacity style={styles.button} onPress={navigateToTerms}>
          <Text style={styles.buttonText}>Terms and Conditions</Text>
          <FontAwesome name="chevron-right" size={18} color="#ffa500" />
        </TouchableOpacity>
        <TouchableOpacity style={styles.button} onPress={navigateToLogout}>
          <Text style={styles.buttonText}>Log out</Text>
          <FontAwesome name="chevron-right" size={18} color="#ffa500" />
        </TouchableOpacity>
      </View>

      <UserBottomTabNavigator navigation={navigation} />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  content: {
    flex: 1,
  },
  button: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingVertical: 15,
    paddingHorizontal: 20,
    borderColor: '#aaa',
    borderBottomWidth: 1,
  },
  buttonText: {
    color: '#444',
    fontSize: 16,
  },
});

export default Setting;
