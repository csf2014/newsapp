// // // import React, { useState, useRef } from 'react';
// // // import { View, Text, TextInput, TouchableOpacity, StyleSheet, Image, ScrollView } from 'react-native';
// // // import * as ImagePicker from 'expo-image-picker';
// // // import { Picker } from '@react-native-picker/picker';
// // // import Header from '../components/Header';
// // // import UserBottomTabNavigator from '../components/userBottomTabNavigator';
// // // import { useNavigation } from '@react-navigation/native';
// // // import { FontAwesome } from '@expo/vector-icons';

// // // const CreateNews = () => {
// // //   const navigation = useNavigation();

// // //   const [title, setTitle] = useState('');
// // //   const [reporterName, setReporterName] = useState('');
// // //   const [category, setCategory] = useState('');
// // //   const [content, setContent] = useState('');
// // //   const [attachments, setAttachments] = useState([]);
// // //   const reporterNameRef = useRef(null);
// // //   const categoryRef = useRef(null);
// // //   const contentRef = useRef(null);

// // //   const handleSubmit = () => {
// // //     console.log('Form submitted');
// // //   };

// // //   const handleFileChange = async () => {
// // //     console.log("Attempting to pick an image...");

// // //     let result = await ImagePicker.launchImageLibraryAsync({
// // //       mediaTypes: ImagePicker.MediaTypeOptions.All,
// // //       allowsEditing: true,
// // //       aspect: [4, 3],
// // //       quality: 1,
// // //     });

// // //     console.log("Image picker result:", result);

// // //     if (!result.canceled && result.assets && result.assets.length > 0) {
// // //       const selectedUri = result.assets[0].uri;
// // //       console.log("Image selected:", selectedUri);
// // //       setAttachments([...attachments, selectedUri]);
// // //     } else {
// // //       console.log("Image picking cancelled or no image selected.");
// // //     }
// // //   };

// // //   const handleClear = () => {
// // //     setTitle('');
// // //     setReporterName('');
// // //     setCategory('');
// // //     setContent('');
// // //     setAttachments([]);
// // //   };

// // //   const handleUploadClick = () => {
// // //     handleFileChange();
// // //   };

// // //   return (
// // //     <View style={{ flex: 1 }}>
// // //       <Header />
// // //       <ScrollView >
// // //         <View style={{ margin: 20 }}>
// // //           <Text style={styles.heading}>Upload News</Text>
// // //           <View style={{ marginBottom: 25 }}>
// // //             <Text style={styles.label}>Title:</Text>
// // //             <TextInput
// // //               style={styles.input}
// // //               value={title}
// // //               onChangeText={setTitle}
// // //               onSubmitEditing={() => reporterNameRef.current.focus()}
// // //             />
// // //           </View>
// // //           <View style={{ marginBottom: 15 }}>
// // //             <Text style={styles.label}>Reporter Name:</Text>
// // //             <TextInput
// // //               style={styles.input}
// // //               value={reporterName}
// // //               onChangeText={setReporterName}
// // //               ref={reporterNameRef}
// // //               onSubmitEditing={() => categoryRef.current.focus()}
// // //             />
// // //           </View>
// // //           <View style={{ marginBottom: 15 }}>
// // //             <Text style={styles.label}>Category:</Text>
// // //             <Picker
// // //               selectedValue={category}
// // //               style={styles.input}
// // //               onValueChange={(itemValue) => setCategory(itemValue)}
// // //               ref={categoryRef}
// // //               onSubmitEditing={() => contentRef.current.focus()}
// // //             >
// // //               <Picker.Item label="Select category..." value="" />
// // //               <Picker.Item label="Announcements" value="announcements" />
// // //               <Picker.Item label="Events" value="events" />
// // //               <Picker.Item label="Clubs" value="clubs" />
// // //             </Picker>
// // //           </View>
// // //           <View style={{ marginBottom: 15 }}>
// // //             <Text style={styles.label}>Content:</Text>
// // //             <TextInput
// // //               style={styles.inputMultiline}
// // //               value={content}
// // //               onChangeText={setContent}
// // //               multiline={true}
// // //               numberOfLines={4}
// // //               ref={contentRef}
// // //             />
// // //           </View>
// // //           <View style={styles.attachment}>
// // //             <TouchableOpacity onPress={handleUploadClick}>
// // //               <FontAwesome name="cloud" size={30} color="#777" style={{ textAlign: "center" }} />
// // //               <Text style={{ color: '#777', marginTop: 5 }}>Tap to upload images/videos</Text>
// // //             </TouchableOpacity>
// // //           </View>
// // //           {attachments.length > 0 && (
// // //             <View style={{ marginTop: 15 }}>
// // //               <Text style={styles.label}>Selected Attachments:</Text>
// // //               <ScrollView horizontal>
// // //                 {attachments.map((uri, index) => (
// // //                   <Image
// // //                     key={index}
// // //                     source={{ uri }}
// // //                     style={styles.attachmentPreview}
// // //                   />
// // //                 ))}
// // //               </ScrollView>
// // //             </View>
// // //           )}
// // //           <View style={{ flexDirection: 'row', justifyContent: 'center' }}>
// // //             <TouchableOpacity onPress={handleClear} style={styles.button}>
// // //               <Text style={{ color: '#ffa500' }}>Clear</Text>
// // //             </TouchableOpacity>
// // //             <TouchableOpacity onPress={handleSubmit} style={styles.button1}>
// // //               <Text style={{ color: '#fff' }}>Post</Text>
// // //             </TouchableOpacity>
// // //           </View>
// // //         </View>
// // //       </ScrollView>
// // //       <UserBottomTabNavigator navigation={navigation} />
// // //     </View>
// // //   );
// // // };

// // // const styles = StyleSheet.create({
// // //   label: {
// // //     margin: 5,
// // //   },
// // //   heading: {
// // //     fontSize: 20,
// // //     textAlign: 'center',
// // //     top: 0,
// // //   },
// // //   input: {
// // //     width: '100%',
// // //     marginTop: 5,
// // //     padding: 8,
// // //     borderWidth: 1,
// // //     borderColor: '#ccc',
// // //     borderRadius: 5,
// // //   },
// // //   inputMultiline: {
// // //     width: '100%',
// // //     height: 140,
// // //     marginTop: 5,
// // //     padding: 8,
// // //     borderWidth: 1,
// // //     borderColor: '#ccc',
// // //     borderRadius: 5,
// // //   },
// // //   attachment: {
// // //     width: '100%',
// // //     height: 100,
// // //     marginTop: 5,
// // //     padding: 30,
// // //     borderWidth: 1,
// // //     borderColor: '#ccc',
// // //     borderRadius: 5,
// // //     justifyContent: 'center',
// // //     alignItems: 'center',
// // //   },
// // //   attachmentPreview: {
// // //     width: 100,
// // //     height: 100,
// // //     marginRight: 10,
// // //     borderRadius: 5,
// // //   },
// // //   button: {
// // //     paddingVertical: 10,
// // //     paddingHorizontal: 50,
// // //     borderRadius: 5,
// // //     borderWidth: 1,
// // //     borderColor: '#ffa500',
// // //     marginVertical: 10,
// // //     marginHorizontal: 5,
// // //     alignItems: 'center',
// // //   },
// // //   button1: {
// // //     paddingVertical: 10,
// // //     paddingHorizontal: 50,
// // //     backgroundColor: '#ffa500',
// // //     borderRadius: 5,
// // //     marginVertical: 10,
// // //     marginHorizontal: 5,
// // //     alignItems: 'center',
// // //   },
// // // });

// // // export default CreateNews;
// // import React, { useState, useRef } from 'react';
// // import { View, Text, TextInput, TouchableOpacity, StyleSheet, Image, ScrollView } from 'react-native';
// // import * as ImagePicker from 'expo-image-picker';
// // import { Picker } from '@react-native-picker/picker';
// // import Header from '../components/Header';
// // import UserBottomTabNavigator from '../components/userBottomTabNavigator';
// // import { useNavigation } from '@react-navigation/native';
// // import { FontAwesome } from '@expo/vector-icons';

// // const CreateNews = () => {
// //   const navigation = useNavigation();

// //   const [title, setTitle] = useState('');
// //   const [reporterName, setReporterName] = useState('');
// //   const [category, setCategory] = useState('');
// //   const [content, setContent] = useState('');
// //   const [attachments, setAttachments] = useState([]);
// //   const reporterNameRef = useRef(null);
// //   const categoryRef = useRef(null);
// //   const contentRef = useRef(null);

// //   const handleSubmit = async () => {
// //     console.log('Form submitted');

// //     try {
// //       const formData = new FormData();
// //       formData.append('title', title);
// //       formData.append('reporter', reporterName); // Correct key name
// //       formData.append('category', category);
// //       formData.append('content', content);

// //       // Assuming only one image is uploaded
// //       if (attachments.length > 0) {
// //         const uriParts = attachments[0].split('.');
// //         const fileType = uriParts[uriParts.length - 1];

// //         formData.append('image', {
// //           uri: attachments[0],
// //           name: `image.${fileType}`,
// //           type: `image/${fileType}`,
// //         });
// //       }

// //       // Example of sending form data to server using fetch
// //       const response = await fetch('http://10.9.92.25:8080/api/news/create', {
// //         method: 'POST',
// //         body: formData,
// //       });

// //       if (response.ok) {
// //         alert('News created successfully');
// //         console.log('News created successfully');
// //         setTitle('');
// //         setReporterName('');
// //         setCategory('');
// //         setContent('');
// //         setAttachments([]);
// //       } else {
// //         throw new Error('Failed to create news' + response.statusText);
// //       }
// //     } catch (error) {
// //       console.error('Error creating news:', error);
// //       alert('Failed to create news' + error.message);
// //     }
// //   };

// //   const handleFileChange = async () => {
// //     console.log("Attempting to pick an image...");

// //     let result = await ImagePicker.launchImageLibraryAsync({
// //       mediaTypes: ImagePicker.MediaTypeOptions.All,
// //       allowsEditing: true,
// //       aspect: [4, 3],
// //       quality: 1,
// //     });

// //     console.log("Image picker result:", result);

// //     if (!result.canceled && result.assets && result.assets.length > 0) {
// //       const selectedUri = result.assets[0].uri;
// //       console.log("Image selected:", selectedUri);
// //       setAttachments([...attachments, selectedUri]);
// //     } else {
// //       console.log("Image picking cancelled or no image selected.");
// //     }
// //   };

// //   const handleClear = () => {
// //     setTitle('');
// //     setReporterName('');
// //     setCategory('');
// //     setContent('');
// //     setAttachments([]);
// //   };

// //   const handleUploadClick = () => {
// //     handleFileChange();
// //   };

// //   return (
// //     <View style={{ flex: 1 }}>
// //       <Header />
// //       <ScrollView >
// //         <View style={{ margin: 20 }}>
// //           <Text style={styles.heading}>Upload News</Text>
// //           <View style={{ marginBottom: 25 }}>
// //             <Text style={styles.label}>Title:</Text>
// //             <TextInput
// //               style={styles.input}
// //               value={title}
// //               onChangeText={setTitle}
// //               onSubmitEditing={() => reporterNameRef.current.focus()}
// //             />
// //           </View>
// //           <View style={{ marginBottom: 15 }}>
// //             <Text style={styles.label}>Reporter Name:</Text>
// //             <TextInput
// //               style={styles.input}
// //               value={reporterName}
// //               onChangeText={setReporterName}
// //               ref={reporterNameRef}
// //               onSubmitEditing={() => categoryRef.current.focus()}
// //             />
// //           </View>
// //           <View style={{ marginBottom: 15 }}>
// //             <Text style={styles.label}>Category:</Text>
// //             <Picker
// //               selectedValue={category}
// //               style={styles.input}
// //               onValueChange={(itemValue) => setCategory(itemValue)}
// //               ref={categoryRef}
// //               onSubmitEditing={() => contentRef.current.focus()}
// //             >
// //               <Picker.Item label="Select category..." value="" />
// //               <Picker.Item label="Announcements" value="announcements" />
// //               <Picker.Item label="Events" value="events" />
// //               <Picker.Item label="Clubs" value="clubs" />
// //             </Picker>
// //           </View>
// //           <View style={{ marginBottom: 15 }}>
// //             <Text style={styles.label}>Content:</Text>
// //             <TextInput
// //               style={styles.inputMultiline}
// //               value={content}
// //               onChangeText={setContent}
// //               multiline={true}
// //               numberOfLines={4}
// //               ref={contentRef}
// //             />
// //           </View>
// //           <View style={styles.attachment}>
// //             <TouchableOpacity onPress={handleUploadClick}>
// //               <FontAwesome name="cloud" size={30} color="#777" style={{ textAlign: "center" }} />
// //               <Text style={{ color: '#777', marginTop: 5 }}>Tap to upload images/videos</Text>
// //             </TouchableOpacity>
// //           </View>
// //           {attachments.length > 0 && (
// //             <View style={{ marginTop: 15 }}>
// //               <Text style={styles.label}>Selected Attachments:</Text>
// //               <ScrollView horizontal>
// //                 {attachments.map((uri, index) => (
// //                   <Image
// //                     key={index}
// //                     source={{ uri }}
// //                     style={styles.attachmentPreview}
// //                   />
// //                 ))}
// //               </ScrollView>
// //             </View>
// //           )}
// //           <View style={{ flexDirection: 'row', justifyContent: 'center' }}>
// //             <TouchableOpacity onPress={handleClear} style={styles.button}>
// //               <Text style={{ color: '#ffa500' }}>Clear</Text>
// //             </TouchableOpacity>
// //             <TouchableOpacity onPress={handleSubmit} style={styles.button1}>
// //               <Text style={{ color: '#fff' }}>Post</Text>
// //             </TouchableOpacity>
// //           </View>
// //         </View>
// //       </ScrollView>
// //       <UserBottomTabNavigator navigation={navigation} />
// //     </View>
// //   );
// // };

// // const styles = StyleSheet.create({
// //   label: {
// //     margin: 5,
// //   },
// //   heading: {
// //     fontSize: 20,
// //     textAlign: 'center',
// //     top: 0,
// //   },
// //   input: {
// //     width: '100%',
// //     marginTop: 5,
// //     padding: 8,
// //     borderWidth: 1,
// //     borderColor: '#ccc',
// //     borderRadius: 5,
// //   },
// //   inputMultiline: {
// //     width: '100%',
// //     height: 140,
// //     marginTop: 5,
// //     padding: 8,
// //     borderWidth: 1,
// //     borderColor: '#ccc',
// //     borderRadius: 5,
// //   },
// //   attachment: {
// //     width: '100%',
// //     height: 100,
// //     marginTop: 5,
// //     padding: 30,
// //     borderWidth: 1,
// //     borderColor: '#ccc',
// //     borderRadius: 5,
// //     justifyContent: 'center',
// //     alignItems: 'center',
// //   },
// //   attachmentPreview: {
// //     width: 100,
// //     height: 100,
// //     marginRight: 10,
// //     borderRadius: 5,
// //   },
// //   button: {
// //     paddingVertical: 10,
// //     paddingHorizontal: 50,
// //     borderRadius: 5,
// //     borderWidth: 1,
// //     borderColor: '#ffa500',
// //     marginVertical: 10,
// //     marginHorizontal: 5,
// //     alignItems: 'center',
// //   },
// //   button1: {
// //     paddingVertical: 10,
// //     paddingHorizontal: 50,
// //     backgroundColor: '#ffa500',
// //     borderRadius: 5,
// //     marginVertical: 10,
// //     marginHorizontal: 5,
// //     alignItems: 'center',
// //   },
// // });

// // export default CreateNews;
// import React, { useState, useRef } from 'react';
// import { View, Text, TextInput, TouchableOpacity, StyleSheet, Image, ScrollView } from 'react-native';
// import * as ImagePicker from 'expo-image-picker';
// import { Picker } from '@react-native-picker/picker';
// import Header from '../components/Header';
// import UserBottomTabNavigator from '../components/userBottomTabNavigator';
// import { useNavigation } from '@react-navigation/native';
// import { FontAwesome } from '@expo/vector-icons';

// const CreateNews = () => {
//   const navigation = useNavigation();

//   const [title, setTitle] = useState('');
//   const [reporterName, setReporterName] = useState('');
//   const [category, setCategory] = useState('');
//   const [content, setContent] = useState('');
//   const [attachments, setAttachments] = useState([]);
//   const reporterNameRef = useRef(null);
//   const categoryRef = useRef(null);
//   const contentRef = useRef(null);

//   const handleSubmit = async () => {
//     console.log('Form submitted');

//     try {
//       const formData = new FormData();
//       formData.append('title', title);
//       formData.append('reporter', reporterName);
//       formData.append('category', category);
//       formData.append('content', content);

//       if (attachments.length > 0) {
//         const uriParts = attachments[0].split('.');
//         const fileType = uriParts[uriParts.length - 1];

//         formData.append('image', {
//           uri: attachments[0],
//           name: `image.${fileType}`,
//           type: `image/${fileType}`,
//         });
//       }

//       const token = localStorage.getItem('token');

//       const response = await fetch('http://10.9.92.25:8080/api/news/create', {
//         method: 'POST',
//         headers: {
//           'Authorization': `Bearer ${token}`,
//         },
//         body: formData,
//       });

//       if (response.ok) {
//         alert('News created successfully');
//         console.log('News created successfully');
//         setTitle('');
//         setReporterName('');
//         setCategory('');
//         setContent('');
//         setAttachments([]);
//       } else {
//         throw new Error('Failed to create news' + response.statusText);
//       }
//     } catch (error) {
//       console.error('Error creating news:', error);
//       alert('Failed to create news' + error.message);
//     }
//   };

//   const handleFileChange = async () => {
//     console.log("Attempting to pick an image...");

//     let result = await ImagePicker.launchImageLibraryAsync({
//       mediaTypes: ImagePicker.MediaTypeOptions.All,
//       allowsEditing: true,
//       aspect: [4, 3],
//       quality: 1,
//     });

//     console.log("Image picker result:", result);

//     if (!result.canceled && result.assets && result.assets.length > 0) {
//       const selectedUri = result.assets[0].uri;
//       console.log("Image selected:", selectedUri);
//       setAttachments([...attachments, selectedUri]);
//     } else {
//       console.log("Image picking cancelled or no image selected.");
//     }
//   };

//   const handleClear = () => {
//     setTitle('');
//     setReporterName('');
//     setCategory('');
//     setContent('');
//     setAttachments([]);
//   };

//   const handleUploadClick = () => {
//     handleFileChange();
//   };

//   return (
//     <View style={{ flex: 1 }}>
//       <Header />
//       <ScrollView >
//         <View style={{ margin: 20 }}>
//           <Text style={styles.heading}>Upload News</Text>
//           <View style={{ marginBottom: 25 }}>
//             <Text style={styles.label}>Title:</Text>
//             <TextInput
//               style={styles.input}
//               value={title}
//               onChangeText={setTitle}
//               onSubmitEditing={() => reporterNameRef.current.focus()}
//             />
//           </View>
//           <View style={{ marginBottom: 15 }}>
//             <Text style={styles.label}>Reporter Name:</Text>
//             <TextInput
//               style={styles.input}
//               value={reporterName}
//               onChangeText={setReporterName}
//               ref={reporterNameRef}
//               onSubmitEditing={() => categoryRef.current.focus()}
//             />
//           </View>
//           <View style={{ marginBottom: 15 }}>
//             <Text style={styles.label}>Category:</Text>
//             <Picker
//               selectedValue={category}
//               style={styles.input}
//               onValueChange={(itemValue) => setCategory(itemValue)}
//               ref={categoryRef}
//               onSubmitEditing={() => contentRef.current.focus()}
//             >
//               <Picker.Item label="Select category..." value="" />
//               <Picker.Item label="Announcements" value="announcements" />
//               <Picker.Item label="Events" value="events" />
//               <Picker.Item label="Clubs" value="clubs" />
//             </Picker>
//           </View>
//           <View style={{ marginBottom: 15 }}>
//             <Text style={styles.label}>Content:</Text>
//             <TextInput
//               style={styles.inputMultiline}
//               value={content}
//               onChangeText={setContent}
//               multiline={true}
//               numberOfLines={4}
//               ref={contentRef}
//             />
//           </View>
//           <View style={styles.attachment}>
//             <TouchableOpacity onPress={handleUploadClick}>
//               <FontAwesome name="cloud" size={30} color="#777" style={{ textAlign: "center" }} />
//               <Text style={{ color: '#777', marginTop: 5 }}>Tap to upload images/videos</Text>
//             </TouchableOpacity>
//           </View>
//           {attachments.length > 0 && (
//             <View style={{ marginTop: 15 }}>
//               <Text style={styles.label}>Selected Attachments:</Text>
//               <ScrollView horizontal>
//                 {attachments.map((uri, index) => (
//                   <Image
//                     key={index}
//                     source={{ uri }}
//                     style={styles.attachmentPreview}
//                   />
//                 ))}
//               </ScrollView>
//             </View>
//           )}
//           <View style={{ flexDirection: 'row', justifyContent: 'center' }}>
//             <TouchableOpacity onPress={handleClear} style={styles.button}>
//               <Text style={{ color: '#ffa500' }}>Clear</Text>
//             </TouchableOpacity>
//             <TouchableOpacity onPress={handleSubmit} style={styles.button1}>
//               <Text style={{ color: '#fff' }}>Post</Text>
//             </TouchableOpacity>
//           </View>
//         </View>
//       </ScrollView>
//       <UserBottomTabNavigator navigation={navigation} />
//     </View>
//   );
// };

// const styles = StyleSheet.create({
//   label: {
//     margin: 5,
//   },
//   heading: {
//     fontSize: 20,
//     textAlign: 'center',
//     top: 0,
//   },
//   input: {
//     width: '100%',
//     marginTop: 5,
//     padding: 8,
//     borderWidth: 1,
//     borderColor: '#ccc',
//     borderRadius: 5,
//   },
//   inputMultiline: {
//     width: '100%',
//     height: 140,
//     marginTop: 5,
//     padding: 8,
//     borderWidth: 1,
//     borderColor: '#ccc',
//     borderRadius: 5,
//   },
//   attachment: {
//     width: '100%',
//     height: 100,
//     marginTop: 5,
//     padding: 30,
//     borderWidth: 1,
//     borderColor: '#ccc',
//     borderRadius: 5,
//     justifyContent: 'center',
//     alignItems: 'center',
//   },
//   attachmentPreview: {
//     width: 100,
//     height: 100,
//     marginRight: 10,
//     borderRadius: 5,
//   },
//   button: {
//     paddingVertical: 10,
//     paddingHorizontal: 50,
//     borderRadius: 5,
//     borderWidth: 1,
//     borderColor: '#ffa500',
//     marginVertical: 10,
//     marginHorizontal: 5,
//     alignItems: 'center',
//   },
//   button1: {
//     paddingVertical: 10,
//     paddingHorizontal: 50,
//     backgroundColor: '#ffa500',
//     borderRadius: 5,
//     marginVertical: 10,
//     marginHorizontal: 5,
//     alignItems: 'center',
//   },
// });

// export default CreateNews;
import React, { useState, useRef } from 'react';
import { View, Text, TextInput, TouchableOpacity, StyleSheet, Image, ScrollView } from 'react-native';
import * as ImagePicker from 'expo-image-picker';
import { Picker } from '@react-native-picker/picker';
import Header from '../components/Header';
import UserBottomTabNavigator from '../components/userBottomTabNavigator';
import { useNavigation } from '@react-navigation/native';
import { FontAwesome } from '@expo/vector-icons';
import AsyncStorage from '@react-native-async-storage/async-storage';

const CreateNews = () => {
  const navigation = useNavigation();

  const [title, setTitle] = useState('');
  const [reporterName, setReporterName] = useState('');
  const [category, setCategory] = useState('');
  const [content, setContent] = useState('');
  const [image, setImage] = useState(null);
  const reporterNameRef = useRef(null);
  const categoryRef = useRef(null);
  const contentRef = useRef(null);

  const handleSubmit = async () => {
    try {
      console.log('Form submitted');

      const formData = new FormData();
      formData.append('title', title);
      formData.append('reporter', reporterName);
      formData.append('category', category);
      formData.append('content', content);
      if (image) {
        const uriParts = image.split('.');
        const fileType = uriParts[uriParts.length - 1];

        formData.append('image', {
          uri: image,
          name: `image.${fileType}`,
          type: `image/${fileType}`,
        });
      }

      // Retrieve the token from AsyncStorage
      const token = await AsyncStorage.getItem('token');

      const response = await fetch('http://10.9.85.243:8080/api/news/create', {
        method: 'POST',
        headers: {
          'Authorization': `Bearer ${token}`,
        },
        body: formData,
      });

      if (response.ok) {
        alert('News created successfully');
        console.log('News created successfully');
        setTitle('');
        setReporterName('');
        setCategory('');
        setContent('');
        setImage(null);
      } else {
        throw new Error('Failed to create news');
      }
    } catch (error) {
      console.error('Error creating news:', error.message);
      alert('Failed to create news');
    }
  };

  const handleFileChange = async () => {
    console.log("Attempting to pick an image...");

    let result = await ImagePicker.launchImageLibraryAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.Images,
      allowsEditing: true,
      aspect: [4, 3],
      quality: 1,
    });

    console.log("Image picker result:", result);

    if (!result.canceled && result.assets && result.assets.length > 0) {
      const selectedUri = result.assets[0].uri;
      console.log("Image selected:", selectedUri);
      setImage(selectedUri);
    } else {
      console.log("Image picking cancelled or no image selected.");
    }
  };

  const handleClear = () => {
    setTitle('');
    setReporterName('');
    setCategory('');
    setContent('');
    setImage(null);
  };

  const handleUploadClick = () => {
    handleFileChange();
  };

  return (
    <View style={{ flex: 1 }}>
      <Header />
      <ScrollView >
        <View style={{ margin: 20 }}>
          <Text style={styles.heading}>Upload News</Text>
          <View style={{ marginBottom: 25 }}>
            <Text style={styles.label}>Title:</Text>
            <TextInput
              style={styles.input}
              value={title}
              onChangeText={setTitle}
              onSubmitEditing={() => reporterNameRef.current.focus()}
            />
          </View>
          <View style={{ marginBottom: 15 }}>
            <Text style={styles.label}>Reporter Name:</Text>
            <TextInput
              style={styles.input}
              value={reporterName}
              onChangeText={setReporterName}
              ref={reporterNameRef}
              onSubmitEditing={() => categoryRef.current.focus()}
            />
          </View>
          <View style={{ marginBottom: 15 }}>
            <Text style={styles.label}>Category:</Text>
            <Picker
              selectedValue={category}
              style={styles.input}
              onValueChange={(itemValue) => setCategory(itemValue)}
              ref={categoryRef}
              onSubmitEditing={() => contentRef.current.focus()}
            >
              <Picker.Item label="Select category..." value="" />
              <Picker.Item label="Announcements" value="announcements" />
              <Picker.Item label="Events" value="events" />
              <Picker.Item label="Clubs" value="clubs" />
            </Picker>
          </View>
          <View style={{ marginBottom: 15 }}>
            <Text style={styles.label}>Content:</Text>
            <TextInput
              style={styles.inputMultiline}
              value={content}
              onChangeText={setContent}
              multiline={true}
              numberOfLines={4}
              ref={contentRef}
            />
          </View>
          <View style={styles.attachment}>
            <TouchableOpacity onPress={handleUploadClick}>
              <FontAwesome name="cloud" size={30} color="#777" style={{ textAlign: "center" }} />
              <Text style={{ color: '#777', marginTop: 5 }}>Tap to upload images/videos</Text>
            </TouchableOpacity>
          </View>
          {image && (
            <View style={{ marginTop: 15 }}>
              <Text style={styles.label}>Selected Image:</Text>
              <Image
                source={{ uri: image }}
                style={styles.attachmentPreview}
              />
            </View>
          )}
          <View style={{ flexDirection: 'row', justifyContent: 'center' }}>
            <TouchableOpacity onPress={handleClear} style={styles.button}>
              <Text style={{ color: '#ffa500' }}>Clear</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={handleSubmit} style={styles.button1}>
              <Text style={{ color: '#fff' }}>Post</Text>
            </TouchableOpacity>
          </View>
        </View>
      </ScrollView>
      <UserBottomTabNavigator navigation={navigation} />
    </View>
  );
};

const styles = StyleSheet.create({
  label: {
    margin: 5,
  },
  heading: {
    fontSize: 20,
    textAlign: 'center',
    top: 0,
  },
  input: {
    width: '100%',
    marginTop: 5,
    padding: 8,
    borderWidth: 1,
    borderColor: '#ccc',
    borderRadius: 5,
  },
  inputMultiline: {
    width: '100%',
    height: 140,
    marginTop: 5,
    padding: 8,
    borderWidth: 1,
    borderColor: '#ccc',
    borderRadius: 5,
  },
  attachment: {
    width: '100%',
    height: 100,
    marginTop: 5,
    padding: 30,
    borderWidth: 1,
    borderColor: '#ccc',
    borderRadius: 5,
    justifyContent: 'center',
    alignItems: 'center',
  },
  attachmentPreview: {
    width: 100,
    height: 100,
    marginRight: 10,
    borderRadius: 5,
  },
  button: {
    paddingVertical: 10,
    paddingHorizontal: 50,
    borderRadius: 5,
    borderWidth: 1,
    borderColor: '#ffa500',
    marginVertical: 10,
    marginHorizontal: 5,
    alignItems: 'center',
  },
  button1: {
    paddingVertical: 10,
    paddingHorizontal: 50,
    backgroundColor: '#ffa500',
    borderRadius: 5,
    marginVertical: 10,
    marginHorizontal: 5,
    alignItems: 'center',
  },
});

export default CreateNews;
