// const User = require('../Model/userModel');
// const nodemailer = require('nodemailer');
// const bcrypt = require('bcrypt');

// const userController = {

//     async signup(req, res) {
//         try {
//             const { name, email, password, confirmPassword } = req.body;

//             // Check if the email is already registered
//             const existingUser = await User.findOne({ email });
//             if (existingUser) {
//                 return res.status(400).json({ error: 'Email is already registered' });
//             }

//             // Check if passwords match
//             if (password !== confirmPassword) {
//                 return res.status(400).json({ error: 'Passwords do not match' });
//             }

//             // Hash the password
//             const hashedPassword = await bcrypt.hash(password, 10); // 10 is the salt rounds

//             // Create a new user with hashed password and verified set to false
//             const user = new User({ name, email, password: hashedPassword, confirmPassword: hashedPassword, verified: false });
//             await user.save();

//             // Generate and send OTP through email
//             const otp = Math.floor(100000 + Math.random() * 900000).toString();
//             user.otp = otp;
//             user.otpExpiration = Date.now() + 600000; // OTP valid for 10 minutes
//             await user.save();

//             const transporter = nodemailer.createTransport({
//                 service: 'Gmail',
//                 auth: {
//                     user: 'sangaychoden2800@gmail.com',
//                     pass: 'brme fzad bcil mxsj'
//                 }
//             });

//             const mailOptions = {
//                 from: 'sangaychoden2800@gmail.com',
//                 to: email,
//                 subject: 'Verification OTP',
//                 text: `Your OTP for signup is ${otp}`
//             };

//             transporter.sendMail(mailOptions, (error, info) => {
//                 if (error) {
//                     console.log('Error sending email:', error);
//                     return res.status(500).json({ error: 'Error sending OTP email' });
//                 } else {
//                     console.log('Email sent:', info.response);
//                     // Send response indicating account creation is pending
//                     res.status(201).json({ message: 'User registration pending. Check your email for OTP.' });
//                 }
//             });
//         } catch (error) {
//             console.log('Error in signup:', error);
//             res.status(500).json({ error: 'Internal server error' });
//         }
//     },


//     async login(req, res) {
//         try {
//             const { email, password } = req.body;

//             // Find the user by email
//             const user = await User.findOne({ email });

//             // Check if user exists
//             if (!user) {
//                 return res.status(400).json({ error: 'User not found' });
//             }

//             // Check if the user's account is verified
//             if (!user.verified) {
//                 return res.status(400).json({ error: 'Account not verified. Please verify your email.' });
//             }

//             // Compare passwords
//             const passwordMatch = await bcrypt.compare(password, user.password);
//             if (!passwordMatch) {
//                 return res.status(400).json({ error: 'Incorrect password' });
//             }

//             // If all checks pass, user is authenticated
//             // You can proceed with generating authentication tokens, session management, etc.
//             res.status(200).json({ message: 'Login successful' });
//         } catch (error) {
//             console.log('Error in login:', error);
//             res.status(500).json({ error: 'Internal server error' });
//         }
//     },

//     // async verifyOTP(req, res) {
//     //     try {
//     //         const { email, otp } = req.body;

//     //         // Find the user by email
//     //         const user = await User.findOne({ email });

//     //         // Check if user exists
//     //         if (!user) {
//     //             return res.status(400).json({ error: 'User not found' });
//     //         }

//     //         // Check if OTP is correct and not expired
//     //         if (user.otp !== otp || Date.now() > user.otpExpiration) {
//     //             return res.status(400).json({ error: 'Invalid or expired OTP' });
//     //         }

//     //         // Update user as verified
//     //         user.verified = true;
//     //         await user.save();

//     //         // Send response indicating account is activated
//     //         res.status(200).json({ message: 'OTP verified successfully. Account activated.' });
//     //     } catch (error) {
//     //         console.log('Error in verifying OTP:', error);
//     //         res.status(500).json({ error: 'Internal server error' });
//     //     }
//     // },
//     async verifyOTP(req, res) {
//         try {
//             const { email, otp } = req.body;

//             // Find the user by email and OTP
//             const user = await User.findOne({ email, otp });

//             // Check if user exists and if the OTP is valid
//             if (!user) {
//                 return res.status(400).json({ error: 'Invalid OTP or email' });
//             }

//             // Check if OTP is expired (assuming otpExpiration is stored in the database)
//             if (user.otpExpiration && Date.now() > user.otpExpiration) {
//                 return res.status(400).json({ error: 'OTP has expired' });
//             }

//             // Update user as verified
//             user.verified = true;
//             await user.save();

//             res.status(200).json({ message: 'OTP verified successfully' });
//         } catch (error) {
//             console.error('Error in verifying OTP:', error);
//             res.status(500).json({ error: 'Internal server error' });
//         }
//     },


// };




// module.exports = userController;
const User = require('../Model/userModel');
const nodemailer = require('nodemailer');
const bcrypt = require('bcrypt');

const userController = {

    async signup(req, res) {
        try {
            const { name, email, password, confirmPassword } = req.body;

            // Check if the email is already registered
            const existingUser = await User.findOne({ email });
            if (existingUser) {
                return res.status(400).json({ error: 'Email is already registered' });
            }

            // Check if passwords match
            if (password !== confirmPassword) {
                return res.status(400).json({ error: 'Passwords do not match' });
            }

            // Hash the password
            const hashedPassword = await bcrypt.hash(password, 10); // 10 is the salt rounds

            // Create a new user with hashed password and verified set to false
            const user = new User({ name, email, password: hashedPassword, confirmPassword: hashedPassword, verified: false });
            await user.save();

            // Generate and send OTP through email
            const otp = Math.floor(100000 + Math.random() * 900000).toString();
            user.otp = otp;
            user.otpExpiration = Date.now() + 600000; // OTP valid for 10 minutes
            await user.save();

            const transporter = nodemailer.createTransport({
                service: 'Gmail',
                auth: {
                    user: 'sangaychoden2800@gmail.com',
                    pass: 'brme fzad bcil mxsj'
                }
            });

            const mailOptions = {
                from: 'sangaychoden2800@gmail.com',
                to: email,
                subject: 'Verification OTP',
                text: `Your OTP for signup is ${otp}`
            };

            transporter.sendMail(mailOptions, (error, info) => {
                if (error) {
                    console.log('Error sending email:', error);
                    return res.status(500).json({ error: 'Error sending OTP email' });
                } else {
                    console.log('Email sent:', info.response);
                    // Send response indicating account creation is pending
                    res.status(201).json({ message: 'User registration pending. Check your email for OTP.' });
                }
            });
        } catch (error) {
            console.log('Error in signup:', error);
            res.status(500).json({ error: 'Internal server error' });
        }
    },


    async login(req, res) {
        try {
            const { email, password } = req.body;

            // Find the user by email
            const user = await User.findOne({ email });

            // Check if user exists
            if (!user) {
                return res.status(400).json({ error: 'User not found' });
            }

            // Check if the user's account is verified
            if (!user.verified) {
                return res.status(400).json({ error: 'Account not verified. Please verify your email.' });
            }

            // Compare passwords
            const passwordMatch = await bcrypt.compare(password, user.password);
            if (!passwordMatch) {
                return res.status(400).json({ error: 'Incorrect password' });
            }

            // If all checks pass, user is authenticated
            // You can proceed with generating authentication tokens, session management, etc.
            res.status(200).json({ message: 'Login successful' });
        } catch (error) {
            console.log('Error in login:', error);
            res.status(500).json({ error: 'Internal server error' });
        }
    },

    async verifyOTP(req, res) {
        try {
            const { email, otp } = req.body;

            // Find the user by email and OTP
            const user = await User.findOne({ email, otp });

            // Check if user exists and if the OTP is valid
            if (!user) {
                return res.status(400).json({ error: 'Invalid OTP or email' });
            }

            // Check if OTP is expired (assuming otpExpiration is stored in the database)
            if (user.otpExpiration && Date.now() > user.otpExpiration) {
                return res.status(400).json({ error: 'OTP has expired' });
            }

            // Update user as verified
            user.verified = true;
            await user.save();

            res.status(200).json({ message: 'OTP verified successfully' });
        } catch (error) {
            console.error('Error in verifying OTP:', error);
            res.status(500).json({ error: 'Internal server error' });
        }
    },

    async resendOTP(req, res) {
        try {
            const { email } = req.body;

            // Find the user by email
            const user = await User.findOne({ email });

            // Check if user exists
            if (!user) {
                return res.status(400).json({ error: 'User not found' });
            }

            // Generate and send new OTP through email
            const otp = Math.floor(100000 + Math.random() * 900000).toString();
            user.otp = otp;
            user.otpExpiration = Date.now() + 600000; // OTP valid for 10 minutes
            await user.save();

            const transporter = nodemailer.createTransport({
                service: 'Gmail',
                auth: {
                    user: 'sangaychoden2800@gmail.com',
                    pass: 'brme fzad bcil mxsj'
                }
            });

            const mailOptions = {
                from: 'sangaychoden2800@gmail.com',
                to: email,
                subject: 'New OTP',
                text: `Your new OTP is ${otp}`
            };

            transporter.sendMail(mailOptions, (error, info) => {
                if (error) {
                    console.log('Error sending email:', error);
                    return res.status(500).json({ error: 'Error sending OTP email' });
                } else {
                    console.log('Email sent:', info.response);
                    res.status(200).json({ message: 'New OTP sent successfully. Check your email.' });
                }
            });
        } catch (error) {
            console.error('Error in resending OTP:', error);
            res.status(500).json({ error: 'Internal server error' });
        }
    },

    async verifyResentOTP(req, res) {
        try {
            const { email, otp } = req.body;

            // Find the user by email and OTP
            const user = await User.findOne({ email, otp });

            // Check if user exists and if the OTP is valid
            if (!user) {
                return res.status(400).json({ error: 'Invalid OTP or email' });
            }

            // Check if OTP is expired (assuming otpExpiration is stored in the database)
            if (user.otpExpiration && Date.now() > user.otpExpiration) {
                return res.status(400).json({ error: 'OTP has expired' });
            }

            // Update user as verified
            user.verified = true;
            await user.save();

            res.status(200).json({ message: 'OTP verified successfully' });
        } catch (error) {
            console.error('Error in verifying resent OTP:', error);
            res.status(500).json({ error: 'Internal server error' });
        }
    },

};

module.exports = userController;
