const News = require('../Model/newsModel');

// Create a new news article
exports.createNews = async (req, res) => {
    try {
        const news = await News.create(req.body);
        res.status(201).json({ news });
    } catch (err) {
        res.status(400).json({ message: err.message });
    }
};

// Get all news articles
exports.getAllNews = async (req, res) => {
    try {
        const news = await News.find();
        res.status(200).json({ news });
    } catch (err) {
        res.status(500).json({ message: err.message });
    }
};
// Get one news article by ID
exports.getNewsById = async (req, res) => {
    try {
        const news = await News.findById(req.params.id);
        if (!news) {
            return res.status(404).json({ message: 'News article not found' });
        }
        res.status(200).json({ news });
    } catch (err) {
        res.status(500).json({ message: err.message });
    }
};

// Update a news article by ID
exports.updateNewsById = async (req, res) => {
    try {
        const news = await News.findByIdAndUpdate(req.params.id, req.body, { new: true });
        if (!news) {
            return res.status(404).json({ message: 'News article not found' });
        }
        res.status(200).json({ news });
    } catch (err) {
        res.status(500).json({ message: err.message });
    }
};

// Delete a news article by ID
exports.deleteNewsById = async (req, res) => {
    try {
        const news = await News.findByIdAndDelete(req.params.id);
        if (!news) {
            return res.status(404).json({ message: 'News article not found' });
        }
        res.status(200).json({ message: 'News article deleted successfully' });
    } catch (err) {
        res.status(500).json({ message: err.message });
    }
};

// Search for news articles by keyword
exports.searchNewsByKeyword = async (req, res) => {
    try {
        const keyword = req.query.keyword;
        const news = await News.find({ $text: { $search: keyword } });
        res.status(200).json({ news });
    } catch (err) {
        res.status(500).json({ message: err.message });
    }
};
// Search for news articles by keyword
// exports.searchNewsByKeyword = async (req, res) => {
//     try {
//         const keyword = req.query.keyword;
//         if (!keyword) {
//             return res.status(400).json({ message: 'Please provide a keyword for search' });
//         }
//         const news = await News.find({
//             $or: [
//                 { title: { $regex: keyword, $options: 'i' } }, // Case-insensitive search for title
//                 { content: { $regex: keyword, $options: 'i' } }, // Case-insensitive search for content
//                 { reporter: { $regex: keyword, $options: 'i' } } // Case-insensitive search for reporter
//             ]
//         });
//         res.status(200).json({ news });
//     } catch (err) {
//         res.status(500).json({ message: err.message });
//     }
// };
