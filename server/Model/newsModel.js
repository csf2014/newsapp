const mongoose = require('mongoose');

const newsSchema = new mongoose.Schema({
    title: {
        type: String,
        required: true,
        index: true // Enable text indexing on the title field
    },
    reporter: {
        type: String,
        required: true
    },
    content: {
        type: String,
        required: true
    },
    image: {
        type: String, // You can store image URL here
        required: true
    },
    category: {
        type: String,
        enum: ['announcements', 'events', 'clubs'],
        required: true
    },
    uploadDate: {
        type: Date,
        default: Date.now
    }
});
newsSchema.index({ title: 'text', content: 'text' }); // Index multiple fields for text search
const News = mongoose.model('News', newsSchema);

module.exports = News;
