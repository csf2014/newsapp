// const mongoose = require('mongoose');
// const Schema = mongoose.Schema;

// const userSchema = new Schema({
//     name: {
//         type: String,
//         required: true
//     },
//     email: {
//         type: String,
//         required: true,
//         unique: true
//     },
//     password: {
//         type: String,
//         required: true
//     },
//     confirmPassword: {
//         type: String,
//         required: true
//     },
//     otp: {
//         type: String,
//         default: null
//     },
//     otpExpiration: {
//         type: Date,
//         default: null
//     }
// });

// const User = mongoose.model('User', userSchema);

// module.exports = User;
// userModel.js

const mongoose = require('mongoose');

const userSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true,
        unique: true
    },
    password: {
        type: String,
        required: true
    },
    confirmPassword: {
        type: String,
        required: true
    },
    verified: {
        type: Boolean,
        default: false // Set default value to false
    },
    otp: {
        type: String
    },
    otpExpiration: {
        type: Date
    }
});

const User = mongoose.model('User', userSchema);

module.exports = User;
