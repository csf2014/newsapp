

// module.exports = router;
const express = require('express');
const router = express.Router();
const newsController = require('../Controller/newsController');

// Search for news articles by keyword
router.get('/search', newsController.searchNewsByKeyword);


// Create a new news article
router.post('/createnews', newsController.createNews);

// Get all news articles
router.get('/', newsController.getAllNews);

// Get one news article by ID
router.get('/:id', newsController.getNewsById);

// Update a news article by ID
router.put('/:id', newsController.updateNewsById);

// Delete a news article by ID
router.delete('/:id', newsController.deleteNewsById);

// // Search for news articles by keyword
// router.get('/search', newsController.searchNewsByKeyword);


// S

module.exports = router;
