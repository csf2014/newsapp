
const express = require('express');
const router = express.Router();
const userController = require('../Controller/userController');

// Signup route
router.post('/signup', userController.signup);

router.post('/verify-otp', userController.verifyOTP);
// router.post('/verify-otp/', userController.verifyOTP);

// Login route
router.post('/login', userController.login);
// router.get('/getEmail', userController.getEmail);
// Route for resending OTP
router.post('/resendOTP', userController.resendOTP);

// Route for verifying resent OTP
router.post('/verifyResentOTP', userController.verifyResentOTP);
module.exports = router;
